package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadAccountStateCustomerToDBAsyncTask extends AsyncTask<Void,Void,Void> {

    private DelegateMainActivity delegateMainActivity = null;

    private Call<AccountStateCustomer> accountStateCustomerCall =  null;

    private Customer customer;

    public LoadAccountStateCustomerToDBAsyncTask(DelegateMainActivity delegateMainActivity,Customer customer) {
        this.delegateMainActivity = delegateMainActivity;
        this.customer = customer;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(accountStateCustomerCall == null){
            accountStateCustomerCall = SuperVeroApiAdapter.getApiService().getCustomerAccount(customer.getId().intValue());
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        accountStateCustomerCall.enqueue(new Callback<AccountStateCustomer>() {
            @Override
            public void onResponse(Call<AccountStateCustomer> call, Response<AccountStateCustomer> response) {
                delegateMainActivity.responseAccountStateCustomer(response,customer);
            }

            @Override
            public void onFailure(Call<AccountStateCustomer> call, Throwable t) {

            }
        });
        return null;
    }
}