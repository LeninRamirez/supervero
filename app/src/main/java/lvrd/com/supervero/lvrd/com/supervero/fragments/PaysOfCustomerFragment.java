package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.adapters.PayOfCustomerAdapter;
import lvrd.com.supervero.lvrd.com.supervero.adapters.SaleOfCustomerAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.PayOfCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleOfCustomer;

public class PaysOfCustomerFragment extends Fragment {

    @BindView(R.id.recyclerViewPaysOfCustomer)
    RecyclerView recyclerViewPaysOfCustomer;

    private ArrayList<PayOfCustomer> paysOfCustomer = new ArrayList<PayOfCustomer>();

    DelegateMainActivity delegateMainActivity;

    public PaysOfCustomerFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delegateMainActivity = (DelegateMainActivity) getActivity();

        this.paysOfCustomer.addAll(delegateMainActivity.getAccountStateCustomer().getPayOfCustomers());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Layout principal
        View view = inflater.inflate(R.layout.fragment_pays_of_customer,container,false);

        // Envolver los componentes del layout
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        PayOfCustomerAdapter payOfCustomerAdapter = new PayOfCustomerAdapter(getContext(),this.paysOfCustomer);
        recyclerViewPaysOfCustomer.setHasFixedSize(true); // Ajustar a un tamaño que no cambiara nunca
        recyclerViewPaysOfCustomer.setLayoutManager(new LinearLayoutManager(getContext())); // Asignar Layout
        recyclerViewPaysOfCustomer.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerViewPaysOfCustomer.setAdapter(payOfCustomerAdapter);  // Asignar adaptador a la lista*/
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
