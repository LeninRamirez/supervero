package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;

import java.util.ArrayList;

import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadSearchProductAsyncTask extends AsyncTask<Void,Void,Void> {

    private DelegateMainActivity delegateMainActivity = null;

    private Call<ArrayList<Product>> productsCall =  null;

    private String nameProduct;

    public LoadSearchProductAsyncTask(DelegateMainActivity delegateMainActivity,String nameProduct) {
        this.delegateMainActivity = delegateMainActivity;
        this.nameProduct = nameProduct;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(productsCall == null){
            productsCall = SuperVeroApiAdapter.getApiService().searchProducts(nameProduct);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        productsCall.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
                delegateMainActivity.responseSearchProducts(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<Product>> call, Throwable t) {

            }
        });

        return null;
    }
}
