package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadSaleLoadedAsyncTask extends AsyncTask<Void,Void,Void> {

    private DelegateMainActivity delegateMainActivity = null;

    private Call<ArrayList<SaleLoaded>> salesLoadedCall =  null;

    public LoadSaleLoadedAsyncTask(DelegateMainActivity delegateMainActivity) {
        this.delegateMainActivity = delegateMainActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(salesLoadedCall == null){
            salesLoadedCall = SuperVeroApiAdapter.getApiService().getSalesLoadedByRoute(MainActivity.route);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        salesLoadedCall.enqueue(new Callback<ArrayList<SaleLoaded>>() {
            @Override
            public void onResponse(Call<ArrayList<SaleLoaded>> call, Response<ArrayList<SaleLoaded>> response) {
                delegateMainActivity.responseSalesLoaded(response);
            }

            @Override
            public void onFailure(Call<ArrayList<SaleLoaded>> call, Throwable t) {
                Log.i("Error",t.toString());
            }
        });
        return null;
    }
}
