package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Sale extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("idSale")
    private Long id;

    private int CustomerId;
    private float TotalAmount;
    private String RealDate;
    private String DeliverPlanDate;
    @SerializedName("CreatedById")
    private int createdById;

    @SerializedName("Items")
    private RealmList<SaleItem> items;

    public Sale() {
        //this.id = SuperVeroApp.SaleID.incrementAndGet();
    }

    public Sale(int customerId, float totalAmount, String realDate, String deliverDate,int createdById) {
        //this.id = SuperVeroApp.SaleID.incrementAndGet();
        CustomerId = customerId;
        TotalAmount = totalAmount;
        RealDate = realDate;
        DeliverPlanDate = deliverDate;
        this.createdById = createdById;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public float getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getRealDate() {
        return RealDate;
    }

    public void setRealDate(String realDate) {
        RealDate = realDate;
    }

    public String getDeliverPlanDate() {
        return DeliverPlanDate;
    }

    public void setDeliverPlanDate(String deliverDate) {
        DeliverPlanDate = deliverDate;
    }

    public int getCreatedById() {
        return createdById;
    }

    public void setCreatedById(int createdById) {
        this.createdById = createdById;
    }

    public RealmList<SaleItem> getItems() {
        return items;
    }

    public void setItems(RealmList<SaleItem> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "CustomerId=" + CustomerId +
                ", TotalAmount=" + TotalAmount +
                ", RealDate=" + RealDate +
                ", DeliverPlanDate=" + DeliverPlanDate +
                ", items=" + items +
                '}';
    }
}
