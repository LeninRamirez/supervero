package lvrd.com.supervero.lvrd.com.supervero.interfaces;

import java.util.ArrayList;

import io.realm.RealmResults;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductAddedShopCar;
import lvrd.com.supervero.lvrd.com.supervero.model.RegisterPayResponse;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import retrofit2.Response;

public interface DelegateMainActivity {

    /**
     * Carga de AsyncTask para ver todos los productos
     * @param response
     */
    public abstract void responseProducts(Response<ArrayList<Product>> response);

    /**
     * Carga de AsynTask para ver todos los clientes
     * @param response
     */
    public abstract void responseCustomers(Response<ArrayList<Customer>> response);


    /**
     * Seleccionar producto de la lista de productos
     * @param product
     */
    public abstract  void selectedProduct(Product product);


    /**
     * Remover producto del carrito de compras
     * @param productAddedShopCar
     */
    public abstract void removeProductShopCar(ProductAddedShopCar productAddedShopCar);

    /**
     * Actualizar producto del carrito de compras
     * @param productAddedShopCar
     */
    public abstract void updateQuantityProductShopCar(ProductAddedShopCar productAddedShopCar);

    /**
     * Seleccionar cliente para cargarle pedido
     * @param customer
     */
    public abstract  void selectedCustomer(Customer customer);

    /**
     * Cargar datos del pedido
     */
    public abstract  void loadSale(String deliverDate);

    /**
     * Carga de AsyncTask para cargar pedido en el servidor
     * @param registerSaleResponse
     */
    public abstract void responseSale(Response<SaleLoaded> registerSaleResponse,boolean isLocal);

    /**
     * Carga de AsyncTask para cargar pago en el servidor
     * @param registerPayResponseResponse
     */
    public abstract void responsePay(Response<RegisterPayResponse> registerPayResponseResponse,boolean onlyPay,boolean isLocal);
    //public abstract void responsePay(Response<ResponseBody> registerPayResponseResponse, boolean onlyPay);

    /**
     * Cargar todos los pedidos del sistema(extraido del ws)
     * @param salesLoaded
     */
    public abstract void responseSalesLoaded(Response<ArrayList<SaleLoaded>> salesLoaded);

    /**
     * Cargar todos los pedidos de un usuario en especifico
     * @param accountStateCustomer
     */
    public abstract void responseAccountStateCustomer(Response<AccountStateCustomer> accountStateCustomer,Customer customer);

    /**
     * Cargar los productos buscados por query
     * @param products
     */
    public abstract void responseSearchProducts(ArrayList<Product> products);

    /**
     * Obtener el cliente seleccionado para levantar el pedido
     * @return
     */
    public abstract Customer getCustomerSelected();

    /**
     * Obtener la respuesta del servidor al cargar un pedido
     * @return
     */
    public  abstract  SaleLoaded getSaleLoaded();

    /**
     * Asignar el estado de cuenta del cliente seleccionado
     * @param accountStateCustomer
     * @return
     */
    public abstract void setAccountStateCustomer(AccountStateCustomer accountStateCustomer);

    /**
     * Obtener el estado de cuenta del cliente seleccionado
     * @return
     */
    public abstract  AccountStateCustomer getAccountStateCustomer();

}
