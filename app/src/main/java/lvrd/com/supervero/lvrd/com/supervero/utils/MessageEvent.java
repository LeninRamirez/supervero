package lvrd.com.supervero.lvrd.com.supervero.utils;

public class MessageEvent {

    public static final byte ADDED_PRODUCT =  0;

    public String mMessage;

    public MessageEvent(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }
}