package lvrd.com.supervero.lvrd.com.supervero.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import es.dmoral.toasty.Toasty;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoginAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.model.User;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoginActivity;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements DelegateLoginActivity {

    //Código de envío entre actividades
    public final static int REQUEST_CODE = 1;

    // Comunicador entre 'LoginActivity' y 'LoginAsyncTask'
    private DelegateLoginActivity delegateLoginActivity = null;

    // Tarea asincrona para realizar login
    private LoginAsyncTask mloginTask = null;

    // UI Cuadro de espera al llamar al login
    private ProgressDialog progressDialog = null;

    // Contexto de la actividad
    Context context = null;

    // Shared Preferences
    SharedPreferences prefs = null;

    /*
    Referencias de elementos graficos en el layout
    */

    // Caja de texto para nombre del usuario
    @BindView(R.id.username_autocompletetext)
    AutoCompleteTextView usernameAutocompletetext;

    // Caja de texto para contraseña del usuario
    @BindView(R.id.password_edittext)
    EditText passwordEdittext;

    // Boton para ejecutar login
    @BindView(R.id.login_button)
    Button loginButton;

    // Formulario del layout
    @BindView(R.id.login_form)
    ScrollView loginForm;

    // Etiqueta de validacion para el nombre del usuario
    @BindView(R.id.username_float_label)
    TextInputLayout usernameFloatLabel;

    // Etiqueta de validacion para la contraseña del usuario
    @BindView(R.id.password_float_label)
    TextInputLayout passwordFloatLabel;

    @BindView(R.id.ib_login_config_url)
    ImageButton configIb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Asociar layout
        setContentView(R.layout.activity_login);

        // Envolver los componentes del layout
        ButterKnife.bind(this);

        // Iniciar el comunicador
        delegateLoginActivity = this;

        // Obtener contexto
        context = this;

        // Asignar contexto al restAPI
        SuperVeroApiAdapter.context = context;


        if(isLogged()){
            goMainActivity(null);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Evento del boton que ejecuta login
     */
    @OnClick(R.id.login_button)
    public void onViewClicked() {
        if(!SuperVeroApiAdapter.getUrlWebService().equals("")){
            attemptLogin();
        }else{
            Toasty.error(context,getString(R.string.error_should_config_app)).show();
        }
    }

    /**
     * Evento para dar "Enter" en el campo password del formulario
     */
    @OnEditorAction(R.id.password_edittext)
    public boolean onEditPassword(int actionId) {
        if (actionId == R.id.login || actionId == EditorInfo.IME_NULL) {
            attemptLogin();
            return true;
        }
        return false;
    }

    /**
     * Ejecucion de login, validacion de campos "username" y "password", llamado de tarea asyncrona
     */
    private void attemptLogin() {
        // Validar que la tarea asyncrona  no se encuentre iniciada
        if (mloginTask != null) {
            return;
        }

        // Si el teclado se encuentra visible se  oculta
        closeKeyboard();

        // Resetea las etiquetas de validacion de la caja de texto "username" y "password".
        usernameFloatLabel.setError(null);
        passwordFloatLabel.setError(null);

        // Extraer nombre usuario de la caja de texto "username"
        String username = usernameAutocompletetext.getText().toString();

        // Extraer contraseña de usuario de la caja de texto "password"
        String password = passwordEdittext.getText().toString();

        // Bandera de validacion, para identificar si hubo algun fallo
        boolean cancel = false;

        // Asociar el componente que no paso la validacion
        View focusView = null;

        // Validar que la contraseña no se encuentre vacia.
        if (TextUtils.isEmpty(password)) {

            // Mostrar en la eiqueta mensaje de validacion
            passwordFloatLabel.setError(getString(R.string.error_field_required));

            // Asociar componente para tener el foco
            focusView = passwordFloatLabel;

            // Cambiar el estado de la bandera de validacion
            cancel = true;

        // Validar que la contraseña sea mayor a 4 caracteres
        } else if (!isPasswordValid(password)) {

            // Mostrar en la eiqueta mensaje de validacion
            passwordFloatLabel.setError(getString(R.string.error_invalid_password));

            // Asociar componente para tener el foco
            focusView = passwordFloatLabel;

            // Cambiar el estado de la bandera de validacion
            cancel = true;
        }

        // Validar que el nombre de usuario no se encuentre vacio.
        if (TextUtils.isEmpty(username)) {

            // Mostrar en la eiqueta mensaje de validacion
            usernameFloatLabel.setError(getString(R.string.error_field_required));

            // Asociar componente para tener el foco
            focusView = usernameFloatLabel;

            // Cambiar el estado de la bandera de validacion
            cancel = true;

            // Validar que el nombre usuario sea un correo electronico
        } else if (!isUserIdValid(username)) {

            // Mostrar en la eiqueta mensaje de validacion
            usernameFloatLabel.setError(getString(R.string.error_invalid_email));

            // Asociar componente para tener el foco
            focusView = usernameFloatLabel;

            // Cambiar el estado de la bandera de validacion
            cancel = true;
        }

        // Validar que "Bandera"
        if (cancel) {
            // Situarse en el componente que no paso las validaciones
            focusView.requestFocus();
        } else {

            // Mostrar Dialogo de progreso de la ejecucion login
            showProgress(true);

            // Creacion de instancia de la Tarea Asincrona para login
            mloginTask = new LoginAsyncTask(delegateLoginActivity,username,password);

            // Ejecucion de la Tarea Asincrona para login
            mloginTask.execute();
        }
    }

    /**
     * Validaion del campo "userName"
     * @param userName
     * @return
     */
    private boolean isUserIdValid(String userName) {
        return (!TextUtils.isEmpty(userName) && Patterns.EMAIL_ADDRESS.matcher(userName).matches());
    }

    /**
     * Validacion del campos "password"
     * @param password
     * @return
     */
    private boolean isPasswordValid(String password) {
        return password.length() >= 4;
    }


    /**
     * Mostrar cuadro de progreso durante la ejecucion de la tarea asincrona de la llama al login
     * @param show
     */
    private void showProgress(boolean show){

        // Conocer si de muestra u oculta formulario de login
        int visibility = show ? View.GONE : View.VISIBLE;

        // Mostrar u ocultar formulario de login
        loginForm.setVisibility(visibility);

        // Si
        if(show) {
            // Instancia cuadro de progreso durante la tarea asincrona
            progressDialog = new ProgressDialog(LoginActivity.this,
                    R.style.progress_bar_style);

            // Asignarle al cuadro de dialogo "loading indeterminate"
            progressDialog.setIndeterminate(true);

            // Establecer mensaje de espera al cuadro de dialogo
            progressDialog.setMessage(getString(R.string.msg_authenticating));

            // Mostrar cuadro de dialogo
            progressDialog.show();
        }
    }

    /**
     * Ocultar teclado del telefono
     */
    private  void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     *
     */
    public boolean isLogged(){
        boolean isLogged = false;
        prefs =  getSharedPreferences(getString(R.string.PREF_APP),Context.MODE_PRIVATE);
        if(!prefs.getString(getString(R.string.PREF_USER_ID),"").isEmpty() &&
                !prefs.getString(getString(R.string.PREF_USER_NAME),"").isEmpty()){
            isLogged = true;
        }
        return isLogged;
    }

    /**
     * Metodo abstracto del comunicador "delegateLoginActivity"
     * Recibe respuesta del webservices
     * @param response
     */
    @Override
    public void responseLogin(Response<User> response) {

        // Finalizar cuadro de progreso durante la ejecucion de la tarea asincrona
        showProgress(false);

        // Limpiar tarea asincrona
        mloginTask = null;

        // Ocultar cuadro de progreso de la  ejecucion de la tarea asincrona
        progressDialog.dismiss();
        progressDialog = null;

        if(response.body() != null){

            // Pasar a la siguiente actividad
            goMainActivity(response);

        }else{
            // Mostrar mensaje de error
            Toasty.error(this, getString(R.string.msg_error_authenticating), Toast.LENGTH_SHORT, true).show();
        }

    }

    @OnClick(R.id.ib_login_config_url)
    public void configUrlWebService(){

        // Obtener datos almacenados
        SharedPreferences appSharedPreferences = getSharedPreferences(getString(R.string.PREF_APP),Context.MODE_PRIVATE);
        String urlWebService = appSharedPreferences.getString(getString(R.string.PREF_URL_WEB_SERVICE), "");

        // Creating alert Dialog with one Button
        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this, R.style.AlertDialogCustom);

        // Setting Dialog Title
        alert.setTitle(getString(R.string.title_configuration_url_web_service));

        // Setting Dialog Message
        alert.setMessage(getString(R.string.msg_configuration_url_web_service));


        final EditText edittext = new EditText(this);
        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(48, 6, 48, 6);
        edittext.setLayoutParams(lp);
        edittext.setGravity(android.view.Gravity.TOP|android.view.Gravity.LEFT);
        edittext.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES|InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        edittext.setLines(1);
        edittext.setMaxLines(1);
        edittext.setText(urlWebService);
        container.addView(edittext,lp);

        alert.setView(container);

        alert.setPositiveButton(getString(R.string.saved_configuration), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //What ever you want to do with the value
                String urlWebService = edittext.getText().toString().trim().toLowerCase();
                SharedPreferences preferences = context.getSharedPreferences(getString(R.string.PREF_APP),MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(getString(R.string.PREF_URL_WEB_SERVICE), urlWebService);
                editor.commit();
                dialog.dismiss();
            }
        });

        alert.setNegativeButton(getString(R.string.button_canceled), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    public void goMainActivity(Response<User> response){
        //Iniciando la actividad Principal
        Intent intent = new Intent(this,MainActivity.class);
        //
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        //Agregar parametros al intent
        intent.putExtra(getString(R.string.PREF_USER_ID), response == null ?  prefs.getString(getString(R.string.PREF_USER_ID),""):String.valueOf(response.body().getId()));
        intent.putExtra(getString(R.string.PREF_USER_NAME),response == null ? prefs.getString(getString(R.string.PREF_USER_NAME),""):response.body().getUsername());
        intent.putExtra(getString(R.string.PREF_USER_ROUTE),response == null ? prefs.getString(getString(R.string.PREF_USER_ROUTE),""): response.body().getRoute());

        //Pasaando a la actividad "MainActivity"
        startActivityForResult(intent,REQUEST_CODE);

        //Agregar animacion a la transicion de las actividades
        overridePendingTransition(R.anim.right_in,R.anim.right_out);
    }
}