package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.model.Sale;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadSaleAsyncTask extends AsyncTask<Void, Void, Void> {

    // API Service
    private Call<SaleLoaded> registerSaleResponse = null;

    private DelegateMainActivity delegateMainActivity = null;

    private Sale sale = null;

    private boolean isLocal = false;

    public LoadSaleAsyncTask(DelegateMainActivity delegateMainActivity,Sale sale){
        this.delegateMainActivity = delegateMainActivity;
        this.sale = sale;
    }

    public LoadSaleAsyncTask(DelegateMainActivity delegateMainActivity,Sale sale,boolean isLocal){
        this.delegateMainActivity = delegateMainActivity;
        this.sale = sale;
        this.isLocal = isLocal;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(registerSaleResponse == null) {
            registerSaleResponse = SuperVeroApiAdapter.getApiService().doSale(sale);
        }

    }

    @Override
    protected Void doInBackground(Void... voids) {
        registerSaleResponse.enqueue(new Callback<SaleLoaded>() {
            @Override
            public void onResponse(Call<SaleLoaded> call, Response<SaleLoaded> response) {
                delegateMainActivity.responseSale(response,isLocal);
            }

            @Override
            public void onFailure(Call<SaleLoaded> call, Throwable t) {
                Log.e("Error",t.toString());
            }
        });
        return null;
    }
}
