package lvrd.com.supervero.lvrd.com.supervero.fragments;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadPayDialogFragment extends DialogFragment {

    @BindView(R.id.textViewAmountTotal_dialogfragment_load_pay)
    TextView textViewAmountTotal;

    @BindView(R.id.textViewLeftTotal_dialogfragment_load_pay)
    TextView textViewLeftTotal;

    @BindView(R.id.editTextQuantityToPay_dialogfragment_load_pay)
    EditText editTextQuantityToPay;

    @BindView(R.id.buttonOk_dialogfragment_load_pay)
    Button buttonOk;

    @BindView(R.id.buttonCancel_dialogfragment_load_pay)
    Button buttonCancel;

    // Pedido Seleccionado
    SaleLoaded saleLoaded;

    // Interface de comunicacion con el MainActivity
    DelegateLoadPay delegateLoadPay;


    /**
     * Interface de comunicacion entre Actividad y DialogFragment
     */
    public interface DelegateLoadPay {
        void setQuantityToPayLoad(float quantity,int customerId,int saleId);
    }


    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static LoadPayDialogFragment newInstance(SaleLoaded saleLoaded) {
        LoadPayDialogFragment f = new LoadPayDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable("saleLoaded",saleLoaded);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Instanciar interface de comunicacion
        delegateLoadPay = (DelegateLoadPay) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        saleLoaded = (SaleLoaded) getArguments().getSerializable("saleLoaded");

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_load_pay, container, false);
        ButterKnife.bind(this,view);



        // Asignar monto a total a pagar
        textViewAmountTotal.setText(getString(R.string.label_textview_amount_total)+Utils.formatMoney(saleLoaded.getTotalAmount()));

        // Asignar el monto pendiente por pagar
        textViewLeftTotal.setText(getString(R.string.label_textview_pending_to_pay)+Utils.formatMoney(saleLoaded.getTotalLeft()));
        return view;
    }

    @OnClick({R.id.buttonCancel_dialogfragment_load_pay})
    public void cancelLoadPay(){
        getDialog().dismiss();
    }

    @OnClick(R.id.buttonOk_dialogfragment_load_pay)
    public void okLoadPay(){
        float quantity = Float.parseFloat(editTextQuantityToPay.getText().toString().isEmpty() ? "0" :editTextQuantityToPay.getText().toString());
        if(quantity != 0.0) {
            delegateLoadPay.setQuantityToPayLoad(quantity, saleLoaded.getCustomer().getId().intValue(), saleLoaded.getId());
            getDialog().dismiss();
        }
    }
}