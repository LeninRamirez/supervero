package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PayOfCustomer extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;

    @SerializedName("Subtotal")
    private float subTotal;

    @SerializedName("PaymentDate")
    private String paymentDate;

    public PayOfCustomer() {
    }

    public PayOfCustomer(int id, float subTotal, String paymentDate) {
        this.id = id;
        this.subTotal = subTotal;
        this.paymentDate = paymentDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        return "PayOfCustomer{" +
                "id=" + id +
                ", subTotal=" + subTotal +
                ", paymentDate='" + paymentDate + '\'' +
                '}';
    }
}