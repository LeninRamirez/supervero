package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;

import java.util.ArrayList;

import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadCustomerToDBAsyncTask extends AsyncTask<Void, Void, Void> {

    // Delegate Activity
    private DelegateMainActivity delegate = null;

    // API Service
    private Call<ArrayList<Customer>> customerCall = null;

    // Construct
    public LoadCustomerToDBAsyncTask(DelegateMainActivity delegate) {
        this.delegate = delegate;;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(customerCall == null) {
            customerCall = SuperVeroApiAdapter.getApiService().getCustomerByRoute(MainActivity.route);
        }
    }
    @Override
    protected Void doInBackground(Void... voids) {
        customerCall.enqueue(new Callback<ArrayList<Customer>>() {
            @Override
            public void onResponse(Call<ArrayList<Customer>> call, Response<ArrayList<Customer>> response) {
                delegate.responseCustomers(response);
            }

            @Override
            public void onFailure(Call<ArrayList<Customer>> call, Throwable t) {

            }
        });
        return null;
    }
}
