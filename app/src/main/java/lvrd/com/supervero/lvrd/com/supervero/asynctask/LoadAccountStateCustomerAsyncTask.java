package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoadAccountStateCustomerDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoadCustomerDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadAccountStateCustomerAsyncTask extends AsyncTask<Void,Void,Void> {

    // Delegate Activity and fragments
    private DelegateLoadAccountStateCustomerDialogFragment delegate = null;

    // API Service
    private Call<AccountStateCustomer> accountStateCustomer = null;

    // Id del cliente
    private int customerId;

    public LoadAccountStateCustomerAsyncTask(DelegateLoadAccountStateCustomerDialogFragment delegateLoadAccountStateCustomerDialogFragment,int customerId){
        this.delegate = delegateLoadAccountStateCustomerDialogFragment;
        this.customerId = customerId;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(accountStateCustomer == null) {
            accountStateCustomer = SuperVeroApiAdapter.getApiService().getCustomerAccount(this.customerId);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        accountStateCustomer.enqueue(new Callback<AccountStateCustomer>() {
            @Override
            public void onResponse(Call<AccountStateCustomer> call, Response<AccountStateCustomer> response) {
                delegate.loadAccountStateCustomer(response.body());
            }

            @Override
            public void onFailure(Call<AccountStateCustomer> call, Throwable t) {

            }
        });
        return null;
    }
}
