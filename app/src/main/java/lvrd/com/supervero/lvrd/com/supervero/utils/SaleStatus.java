package lvrd.com.supervero.lvrd.com.supervero.utils;

import com.google.gson.annotations.SerializedName;

public enum SaleStatus {
    PAGADO("Pagado"),
    PENDIENTE("Pendiente"),
    CANCELADO("Cancelado");

    private final String Status;

    SaleStatus(String Status){
        this.Status = Status;
    }

    public String getStatus() {
        return Status;
    }
}
