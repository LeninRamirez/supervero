package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmField;

public class SaleLoaded extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;
    private float TotalAmount;
    private float TotalLeft;
    private String RealDate;
    private String DeliverPlanDate;

    @SerializedName("DeliverDate")
    private String deliverDate;

    private int CustomerId;


    @SerializedName("Products")
    @RealmField(name = "relation_product")
    private RealmList<Product> productsOfSaleLoaded;

    @SerializedName("Status")
    private String status;

    @SerializedName("Customer")
    private Customer customer;

    @SerializedName("Payments")
    private RealmList<Payment> payments;

    public SaleLoaded() {
    }

    public SaleLoaded(int id, float totalAmount, float totalLeft, String realDate, String deliverPlanDate, int customerId, String status, Customer customer,RealmList<Product> productsOfSaleLoaded) {
        this.id = id;
        TotalAmount = totalAmount;
        TotalLeft = totalLeft;
        RealDate = realDate;
        DeliverPlanDate = deliverPlanDate;
        CustomerId = customerId;
        this.status = status;
        this.customer = customer;
        this.productsOfSaleLoaded = productsOfSaleLoaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        TotalAmount = totalAmount;
    }

    public float getTotalLeft() {
        return TotalLeft;
    }

    public void setTotalLeft(float totalLeft) {
        TotalLeft = totalLeft;
    }

    public String getRealDate() {
        return RealDate;
    }

    public void setRealDate(String realDate) {
        RealDate = realDate;
    }

    public String getDeliverPlanDate() {
        return DeliverPlanDate;
    }

    public void setDeliverPlanDate(String deliverDate) {
        DeliverPlanDate = deliverDate;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public RealmList<Product> getProductsOfSaleLoaded() {
        return productsOfSaleLoaded;
    }

    public void setProductsOfSaleLoaded(RealmList<Product> productsOfSaleLoaded) {
        this.productsOfSaleLoaded = productsOfSaleLoaded;
    }

    public RealmList<Payment> getPayments() {
        return payments;
    }

    public void setPayments(RealmList<Payment> payments) {
        this.payments = payments;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }

    @Override
    public String toString() {
        return "SaleLoaded{" +
                "id=" + id +
                ", TotalAmount=" + TotalAmount +
                ", TotalLeft=" + TotalLeft +
                ", RealDate='" + RealDate + '\'' +
                ", DeliverPlanDate='" + DeliverPlanDate + '\'' +
                ", deliverDate='" + deliverDate + '\'' +
                ", CustomerId=" + CustomerId +
                ", productsOfSaleLoaded=" + productsOfSaleLoaded +
                ", status='" + status + '\'' +
                ", customer=" + customer +
                ", payments=" + payments +
                '}';
    }
}
