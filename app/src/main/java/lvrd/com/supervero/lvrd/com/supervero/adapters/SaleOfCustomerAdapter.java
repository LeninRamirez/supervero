package lvrd.com.supervero.lvrd.com.supervero.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleOfCustomer;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class SaleOfCustomerAdapter extends RecyclerView.Adapter<SaleOfCustomerAdapter.SaleOfCustomerViewHolder> {

    //Lista de pagos del cliente
    private ArrayList<SaleOfCustomer> salesOfCustomer = null;

    // Context
    Context context;

    public SaleOfCustomerAdapter(Context context,ArrayList<SaleOfCustomer> salesOfCustomer) {
        this.context = context;
        this.salesOfCustomer = salesOfCustomer;
    }

    @Override
    public SaleOfCustomerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // Cargar layout
        View view = LayoutInflater.from(context).inflate(R.layout.recycle_view_sale_of_customer_item,viewGroup,false);
        SaleOfCustomerAdapter.SaleOfCustomerViewHolder viewHolder = new SaleOfCustomerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SaleOfCustomerViewHolder saleOfCustomerViewHolder, int i) {

        // Asociar los pagos
        saleOfCustomerViewHolder.bind(salesOfCustomer.get(i));
    }

    @Override
    public int getItemCount() {
        return salesOfCustomer.size();
    }

    public class SaleOfCustomerViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textViewTotalAmountOfCustomer)
        TextView textViewTotalAmountOfCustomer;

        //@BindView(R.id.textViewDeliverPlanDateOfCustomer)
        //TextView textViewDeliverPlanDateOfCustomer;

        @BindView(R.id.textViewStatusOfCustomer)
        TextView textViewStatusOfCustomer;

        //@BindView(R.id.textViewDeliverDateOfCustomer)
        //TextView textViewDeliverDateOfCustomer;

        @BindView(R.id.textViewRealDateOfCustomer)
        TextView textViewRealDateOfCustomer;

        @BindView(R.id.textViewIdSaleOfCustomer)
        TextView textViewIdSaleOfCustomer;

        public SaleOfCustomerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(SaleOfCustomer saleOfCustomer){
            textViewTotalAmountOfCustomer.setText(Utils.formatMoney(saleOfCustomer.getTotalAmount()));
            //textViewDeliverPlanDateOfCustomer.setText(Utils.formatDate(saleOfCustomer.getDeliverPlanDate()));
            textViewStatusOfCustomer.setText(saleOfCustomer.getStatus());
            //textViewDeliverDateOfCustomer.setText(saleOfCustomer.getDeliverDate());
            textViewRealDateOfCustomer.setText(Utils.formatDate(saleOfCustomer.getRealDate()));
            textViewIdSaleOfCustomer.setText("No. "+saleOfCustomer.getId());
        }
    }
}

