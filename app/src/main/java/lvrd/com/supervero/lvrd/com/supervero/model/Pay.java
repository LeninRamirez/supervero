package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lvrd.com.supervero.lvrd.com.supervero.app.SuperVeroApp;

public class Pay extends RealmObject implements Serializable {

    @PrimaryKey
    @Expose
    @SerializedName("payId")
    private int id;

    @SerializedName("Subtotal")
    private float Subtotal;
    private int CustomerId;

    @SerializedName("CreatedById")
    private int createdById;

    private int saleId;

    public Pay() {
        this.id = SuperVeroApp.PayID.incrementAndGet();
    }

    public Pay(float subtotal, int customerId) {

        Subtotal = subtotal;
        CustomerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(float subtotal) {
        Subtotal = subtotal;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        CustomerId = customerId;
    }

    public int getCreatedById() {
        return createdById;
    }

    public void setCreatedById(int createdById) {
        this.createdById = createdById;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    @Override
    public String toString() {
        return "Pay{" +
                "Subtotal=" + Subtotal +
                ", CustomerId=" + CustomerId +
                ", createdById=" + createdById +
                '}';
    }
}
