package lvrd.com.supervero.lvrd.com.supervero.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;
import okhttp3.internal.Util;

public class SplashScreenActivity extends AppCompatActivity {

    // Shared preferences
    private SharedPreferences sharedPreferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(getString(R.string.PREF_APP),Context.MODE_PRIVATE);

        Intent intentLogin = new Intent(this, LoginActivity.class);
        Intent intentMain = new Intent(this, MainActivity.class);

        if (!TextUtils.isEmpty(Utils.getSharedPreferences(sharedPreferences,getString(R.string.PREF_USER_NAME))) &&
                !TextUtils.isEmpty(Utils.getSharedPreferences(sharedPreferences,getString(R.string.PREF_USER_PASSWORD)))) {
            startActivity(intentMain);
        } else {
            startActivity(intentLogin);
        }
        finish();
    }
}
