package lvrd.com.supervero.lvrd.com.supervero.model;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Customer extends RealmObject implements Serializable {

    @PrimaryKey
    private Long id;
    private String SocialName;
    private String ShortName;
    private String RFC;
    private String Phone;
    private String Email;
    private String LimitMoney;

    public Customer() {
    }

    public Customer(Long id, String socialName, String shortName, String RFC, String phone, String email) {
        this.id = id;
        SocialName = socialName;
        ShortName = shortName;
        this.RFC = RFC;
        Phone = phone;
        Email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSocialName() {
        return SocialName;
    }

    public void setSocialName(String socialName) {
        SocialName = socialName;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLimitMoney() {
        return LimitMoney;
    }

    public void setLimitMoney(String limitMoney) {
        LimitMoney = limitMoney;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", SocialName='" + SocialName + '\'' +
                ", ShortName='" + ShortName + '\'' +
                ", RFC='" + RFC + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Email='" + Email + '\'' +
                ", LimitMoney='" + LimitMoney + '\'' +
                '}';
    }
}
