package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductAddedShopCar;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.utils.Printer;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class PayFragment extends Fragment {

    @BindView(R.id.tv_pay_total)
    TextView tvPayTotal;

    @BindView(R.id.et_pay_quantity_to_pay)
    EditText etPayQuantityToPay;

    @BindView(R.id.et_pay_deliver_date)
    EditText etPayDeliverDate;

    @BindView(R.id.bt_pay_save_order)
    Button btPaySaveOrder;

    @BindView(R.id.bt_pay_print_order)
    Button btPayPrintOrder;

    @BindView(R.id.tilPlannedDate)
    TextInputLayout tilPlannedDate;

    // Comunicacion entre fragments
    public DelegateMainActivity delegateMainActivity;

    // Cuadro de progreso cuando se manda el pedido
    private ProgressDialog dialogProgress;

    // Formato para asignar a fecha
    SimpleDateFormat formateDate = new SimpleDateFormat("yyyy-MM-dd");

    // Objeto para imprimir tickets
    Printer  printer = null;

    // Array de los productos agregados al carrito
    ArrayList<ProductAddedShopCar> productAddedShopCars;

    // Contexto
    Context context;


    public PayFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delegateMainActivity = (DelegateMainActivity) getActivity();
        dialogProgress = new ProgressDialog(context);
        dialogProgress.setMessage(getString(R.string.msg_load_sale_success));

        printer = new Printer((Activity)context);

        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Layout principal
        View view = inflater.inflate(R.layout.fragment_pay,container,false);

        // Envolver los componentes del layout
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Evento click sobre el boton "Guardar Pedido"
     */
    @OnClick(R.id.bt_pay_save_order)
    public void saveOrder(){
        if(etPayDeliverDate.getText().toString().equals("")){
            tilPlannedDate.setError(getString(R.string.error_msg_not_set_date_planned));
            tilPlannedDate.requestFocus();
            return;
        }
        String pay = etPayQuantityToPay.getText().toString();
        String deliverDate = etPayDeliverDate.getText().toString();
        dialogProgress.show();
        delegateMainActivity.loadSale(deliverDate);
        ((MainActivity)getActivity()).closeKeyboard();
    }

    /**
     * Evento click sobre el boton "Imprimir Pedido"
     */
    @OnClick(R.id.bt_pay_print_order)
    public void printOrder(){


        try {
            printer.findBluetoothDevice();
            printer.openBluetoothPrinter();
            printer.printData(buildTicket());
            printer.disconnectBT();
        } catch (IOException | NullPointerException e) {
            // Mostrar mensaje de error
            Toasty.error(context, getString(R.string.error_msg_printer), Toast.LENGTH_SHORT, true).show();
        }
    }

    /**
     * Evento click sobre el campo "Fecha de Entrega"
     */
    @OnClick(R.id.et_pay_deliver_date)
    public void showDatePickerDialog() {

        DatePickerDialogFragment newFragment = DatePickerDialogFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                // +1 because january is zero
                final String selectedDate = year + "-" + twoDigits(month + 1) + "-" + twoDigits(day);

                if(isValidDate(selectedDate)) {
                    etPayDeliverDate.setText(selectedDate);
                    tilPlannedDate.setError(null);
                }else{
                    etPayDeliverDate.setText("");
                    tilPlannedDate.setError(getString(R.string.error_msg_not_validate_date_planned));
                }
            }
        });
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }

    /**
     * Obtener valor del campo aportacion
     * @return
     */
    public EditText getEtPayQuantityToPay() {
        return etPayQuantityToPay;
    }

    /**
     * Ocultar el cuadro de progreso
     */
    public void closeDialogProgress(){
        dialogProgress.dismiss();
    }

    /**
     * Asignar y mostrar el total del pedido
     * @param totalOrder
     */
    public void setTotalOrder(float totalOrder){
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String result = format.format(totalOrder);
        tvPayTotal.setText(getString(R.string.text_total_to_pay)+result);
    }

    /**
     * Limpiar todos los datos del fragment
     */
    public void clearFragment(){
        tvPayTotal.setText("-----");
        etPayDeliverDate.setText("");
        etPayQuantityToPay.setText("");
    }

    /**
     * Asignar la lista productos
     * @param productAddedShopCars
     */
    public void setProductAddedShopCars(ArrayList<ProductAddedShopCar> productAddedShopCars) {
        this.productAddedShopCars = productAddedShopCars;
    }

    /**
     * Dar formato a la fecha
     * @param n
     * @return
     */
    private String twoDigits(int n) {
        return (n<=9) ? ("0"+n) : String.valueOf(n);
    }

    /**
     * Validar fecha seleccionada por el usuario
     * @param date
     * @return
     */
    private boolean isValidDate(String date){
        boolean isValid = false;
        Date dateToValidate = null;
        try {
            dateToValidate = formateDate.parse(date);
            Date currentDate = new Date();
            int compare = currentDate.compareTo(dateToValidate);
            if(compare  <= 0){
                isValid = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return isValid;
    }

    private String buildTicket(){
        StringBuilder builderTicket = new StringBuilder();

        // Obtener cliente seleccionado para levantar el pedido
        Customer customer = delegateMainActivity.getCustomerSelected();

        // Obtener respuesta del servidor al cargar el pedido
        SaleLoaded saleLoaded = delegateMainActivity.getSaleLoaded();

        builderTicket.append("      ..:: Super Vero ::.. \n");
        builderTicket.append("Cliente : "+customer.getSocialName()+"\n");
        builderTicket.append("Correo : "+customer.getEmail()+"\n");
        builderTicket.append("Tel :"+customer.getPhone()+"\n");
        builderTicket.append("Ruta : "+MainActivity.route+"\n");
        builderTicket.append("Vendedor : "+MainActivity.userName+"\n");

        if(saleLoaded != null) {
            builderTicket.append("Fecha del Pedido : " + Utils.formatDate(saleLoaded.getRealDate()) + "\n");
            builderTicket.append("\n");
            builderTicket.append("PEDIDO # : " + saleLoaded.getId() + "\n");
        }
        builderTicket.append("\n");
        for(ProductAddedShopCar productAddedShopCar : this.productAddedShopCars){
            builderTicket.append(String.format("%3d",productAddedShopCar.getQuantity()));
            builderTicket.append("  ");
            builderTicket.append(productAddedShopCar.getUnit());
            builderTicket.append("  ");
            builderTicket.append(String.format("%5s",productAddedShopCar.getName()));
            builderTicket.append(" ");
            builderTicket.append(Utils.formatMoney(productAddedShopCar.getSellPrice()));
            builderTicket.append(" ");
            builderTicket.append(Utils.formatMoney(productAddedShopCar.getTotal()));
            builderTicket.append("\n");
        }

        builderTicket.append("\n");

        //
        if(saleLoaded != null) {
            float subTotal = (saleLoaded.getTotalAmount() * 25) / 29;

            builderTicket.append("SubTotal :" + Utils.formatMoney(subTotal) + "\n");
            builderTicket.append("Iva :" + Utils.formatMoney((subTotal * Float.parseFloat("0.16"))) + "\n");
            builderTicket.append("Total :" + Utils.formatMoney(saleLoaded.getTotalAmount()) + "\n");
        }

        builderTicket.append("\n");
        builderTicket.append("Anticipo : "+Utils.formatMoney(Float.parseFloat(etPayQuantityToPay.getText().toString()))+"\n");

        builderTicket.append("\n");
        if(saleLoaded != null) {
            builderTicket.append("Fecha de Solicitud de Entrega\n");
            builderTicket.append("           " + Utils.formatDate(saleLoaded.getDeliverPlanDate()) + "\n");
        }else{
            builderTicket.append("      *** VENTA PENDIENTE POR SINCRONIZAR ***");
        }

        builderTicket.append("\n");
        builderTicket.append("      Domicilio de Entrega\n\n\n");
        builderTicket.append("        ________________\n");

        builderTicket.append("\n");
        builderTicket.append("        Firma del Cliente\n\n\n");
        builderTicket.append("        _________________\n");

        return builderTicket.toString();
    }
}
