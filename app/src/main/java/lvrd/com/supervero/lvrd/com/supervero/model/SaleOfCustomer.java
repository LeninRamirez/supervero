package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SaleOfCustomer extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;

    @SerializedName("TotalAmount")
    private float totalAmount;

    @SerializedName("RealDate")
    private String realDate;

    @SerializedName("DeliverPlanDate")
    private String deliverPlanDate;

    @SerializedName("DeliverDate")
    private String deliverDate;

    @SerializedName("Status")
    private String status;

    public SaleOfCustomer() {
    }

    public SaleOfCustomer(int id, float totalAmount, String realDate, String deliverPlanDate, String deliverDate, String status) {
        this.id = id;
        this.totalAmount = totalAmount;
        this.realDate = realDate;
        this.deliverPlanDate = deliverPlanDate;
        this.deliverDate = deliverDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getRealDate() {
        return realDate;
    }

    public void setRealDate(String realDate) {
        this.realDate = realDate;
    }

    public String getDeliverPlanDate() {
        return deliverPlanDate;
    }

    public void setDeliverPlanDate(String deliverPlanDate) {
        this.deliverPlanDate = deliverPlanDate;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SaleOfCustomer{" +
                "id=" + id +
                ", totalAmount=" + totalAmount +
                ", realDate='" + realDate + '\'' +
                ", deliverPlanDate='" + deliverPlanDate + '\'' +
                ", deliverDate='" + deliverDate + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}