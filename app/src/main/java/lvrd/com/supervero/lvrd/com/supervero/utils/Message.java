package lvrd.com.supervero.lvrd.com.supervero.utils;

import android.content.Context;
import android.widget.Toast;
import es.dmoral.toasty.Toasty;

/**
 * Clase estatica para mostrar mensajes(success,error,warning,info,normal) de notificacion en las acciones
 */
public final class Message {

    /**
     * Constructor privado
     * Configura las propiedades de los objetos mensajes... como tamaño de letra,colores para los diferentes tipos de mensajes
     */
    private Message(){
        Toasty.Config.getInstance()
        .setTextSize(12) // Tamaño de letra en el mensaje
        .apply();
    }

    /**
     * Muestra mensaje de error
     * @param context
     * @param message
     */
    public static void errorMessage(Context context, String message){
        Toasty.error(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Muestra mensaje de realizado(success)
     * @param context
     * @param message
     */
    public static void successMessage(Context context, String message){
        Toasty.success(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Muestra mensaje de informacion(info)
     * @param context
     * @param message
     */
    public static void infoMessage(Context context, String message){
        Toasty.info(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Muestra mensaje de alerta(warning)
     * @param context
     * @param message
     */
    public static void warningMessage(Context context, String message){
        Toasty.warning(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Muestra mensaje normal
     * @param context
     * @param message
     */
    public static void normalMessage(Context context, String message){
        Toasty.normal(context, message).show();
    }
}