package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AccountStateCustomer extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;

    @SerializedName("TotalSales")
    private float totalSales;

    @SerializedName("TotalPayments")
    private float totalPayments;

    private Customer customer;

    @SerializedName("PaymentsList")
    private RealmList<PayOfCustomer> payOfCustomers;

    @SerializedName("SalesList")
    private RealmList<SaleOfCustomer> saleOfCustomers;

    public AccountStateCustomer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(float totalSales) {
        this.totalSales = totalSales;
    }

    public float getTotalPayments() {
        return totalPayments;
    }

    public void setTotalPayments(float totalPayments) {
        this.totalPayments = totalPayments;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public RealmList<PayOfCustomer> getPayOfCustomers() {
        return payOfCustomers;
    }

    public void setPayOfCustomers(RealmList<PayOfCustomer> payOfCustomers) {
        this.payOfCustomers = payOfCustomers;
    }

    public RealmList<SaleOfCustomer> getSaleOfCustomers() {
        return saleOfCustomers;
    }

    public void setSaleOfCustomers(RealmList<SaleOfCustomer> saleOfCustomers) {
        this.saleOfCustomers = saleOfCustomers;
    }

    @Override
    public String toString() {
        return "AccountStateCustomer{" +
                "id=" + id +
                ", totalSales=" + totalSales +
                ", totalPayments=" + totalPayments +
                ", customer=" + customer +
                ", payOfCustomers=" + payOfCustomers +
                ", saleOfCustomers=" + saleOfCustomers +
                '}';
    }
}