package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.app.SuperVeroApp;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadCustomerAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoadCustomerDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;

public class SelectedCustomerDialogFragment extends DialogFragment implements /*SearchView.OnQueryTextListener,*/DelegateLoadCustomerDialogFragment {
    
    @BindView(R.id.rv_customers)
    RecyclerView customerRv;

    @BindView(R.id.pb_loading_customers)
    ProgressBar loadingCustomersPb;

    @BindView(R.id.bt_selected_customer)
    Button selectedCustomerBt;

    @BindView(R.id.searchViewCustomer)
    SearchView searchViewCustomer;

    // Tarea asincrona para realizar la carga de clientes
    private LoadCustomerAsyncTask mLoadCustomerAsyncTask = null;

    SelectedCustomerAdapter selectedCustomerAdapter;

    private ArrayList<Customer> mCustomers = new ArrayList<Customer>();

    DelegateLoadCustomerDialogFragment delegateDialog = null;

    DelegateMainActivity delegateMainActivity;

    // Instancia para la base de datos
    Realm realm = null;

    public static SelectedCustomerDialogFragment newInstance(){
        SelectedCustomerDialogFragment f = new SelectedCustomerDialogFragment();

        Bundle args = new Bundle();
        //args.putSerializable(CUSTOMERS, (Serializable) customers);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delegateDialog =  this;
        delegateMainActivity = (DelegateMainActivity) context;

        // Inicio la instancia para la base de datos
        Realm.init(context);

        try{
            realm = Realm.getDefaultInstance();

        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);

        }
        // Obtener la instancia de la base de datos Realm
        //realm = Realm.getDefaultInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //customers = (ArrayList<Customer>) getArguments().getSerializable(CUSTOMERS);

    }


    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.dialogfragment_customers,container,false);

        ButterKnife.bind(this, view);

        //SET TITLE DIALOG TITLE
        getDialog().setTitle("Clientes");

        // Asociar un administrador de layout
        customerRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        selectedCustomerAdapter = new SelectedCustomerAdapter(mCustomers,getActivity(),delegateMainActivity);
        customerRv.setAdapter(selectedCustomerAdapter);

        //customerSv.setOnQueryTextListener(this);
        if(!MainActivity.mode_offline_app) {
            if (mLoadCustomerAsyncTask == null) {
                mLoadCustomerAsyncTask = new LoadCustomerAsyncTask(delegateDialog);
                mLoadCustomerAsyncTask.execute();
            }
        }else{
            // Llamar a la base de datos
            this.getAllCustomersFromDataBase();
        }

        //BUTTON
        selectedCustomerBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                delegateMainActivity.selectedCustomer(selectedCustomerAdapter.getCustomerSelected());
                dismiss();
            }
        });

        // Search View Customer on ArrayList
        searchViewCustomer.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                selectedCustomerAdapter.getFilter().filter(newText.toLowerCase());
                return false;
            }
        });
        return view;

    }

    /*@Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        selectedCustomerAdapter.getFilter().filter(newText);
        return false;
    }*/

    @Override
    public void loadCustomers(ArrayList<Customer> response) {

        loadingCustomersPb.setVisibility(View.INVISIBLE);
        searchViewCustomer.setVisibility(View.VISIBLE);
        for (Customer customer : response){
            mCustomers.add(customer);
        }
        selectedCustomerAdapter.notifyDataSetChanged();
    }

    /**
     * Obtener todos los registros de clientes desde la base de datos
     */
    private void getAllCustomersFromDataBase(){
        Iterator<Customer> customersDB = realm.where(Customer.class).findAll().iterator();
        ArrayList<Customer> customers = new ArrayList<Customer>();

        while (customersDB.hasNext()){
            customers.add(customersDB.next());
        }
        loadCustomers(customers);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
