package lvrd.com.supervero.lvrd.com.supervero.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class SeeProductAdapter extends RecyclerView.Adapter<SeeProductAdapter.ViewHolder> {

    private int layout;
    private ArrayList<Product> products;
    private Context context;

    public SeeProductAdapter(ArrayList<Product> products,int layout, Context context) {
        this.layout = layout;
        this.products = products;
        this.context = context;
    }

    @Override
    public SeeProductAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(layout,viewGroup,false);
        ViewHolder viewHolder= new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SeeProductAdapter.ViewHolder viewHolder, int i) {
        viewHolder.bind(this.products.get(i));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.textViewName_recycle_view_see_product_item)
        TextView textViewName;

        @BindView(R.id.textViewQuantity_recycle_view_see_product_item)
        TextView textViewQuantity;

        @BindView(R.id.textViewSubTotal_recycle_view_see_product_item)
        TextView textViewSubTotal;

        @BindView(R.id.textViewType_recycle_view_see_product_item)
        TextView textViewType;

        @BindView(R.id.textViewUnit_recycle_view_see_product_item)
        TextView textViewUnit;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(Product product) {
            textViewName.setText(product.getName());
            textViewQuantity.setText(context.getString(R.string.label_quantity)+product.getQuantity()+"");
            textViewType.setText(context.getString(R.string.label_type)+product.getType());
            textViewUnit.setText(context.getString(R.string.label_unit)+product.getUnit());
            textViewSubTotal.setText(context.getString(R.string.label_subtotal)+Utils.formatMoney(product.getSubtotal())+"");
        }

    }
}
