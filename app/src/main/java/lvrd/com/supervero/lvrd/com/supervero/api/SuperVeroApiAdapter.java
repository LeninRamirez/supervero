package lvrd.com.supervero.lvrd.com.supervero.api;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.concurrent.TimeUnit;

import lvrd.com.supervero.R;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SuperVeroApiAdapter {

    private static SuperVeroApiService API_SERVICE;
    public static Context context;


    public static SuperVeroApiService getApiService() {

        // Creamos un interceptor y le indicamos el log level a usar
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        String baseUrl = SuperVeroApiAdapter.getUrlWebService();//"http://173.249.17.194:5001/";

        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build()) // <-- usamos el log level
                    .build();
            API_SERVICE = retrofit.create(SuperVeroApiService.class);
        }

        return API_SERVICE;
    }

    public static String getUrlWebService(){
        SharedPreferences preferences = context.getSharedPreferences(context.getString(R.string.PREF_APP),context.MODE_PRIVATE);
        return preferences.getString(context.getString(R.string.PREF_URL_WEB_SERVICE),"");
    }
}
