package lvrd.com.supervero.lvrd.com.supervero.model;

import java.io.Serializable;

public class ProductAddedShopCar implements Serializable {
    private int id;
    private String name;
    private float sellPrice;
    private int quantity;
    private String unit;
    private float total;


    public ProductAddedShopCar() {
    }

    public ProductAddedShopCar(int id, String name, float sellPrice, int quantity,String unit) {
        this.id = id;
        this.name = name;
        this.sellPrice = sellPrice;
        this.quantity = quantity;
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotal() {
        total = this.quantity * this.sellPrice;
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        unit = unit;
    }

    @Override
    public String toString() {
        return "ProductAddedShopCar{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sellPrice=" + sellPrice +
                ", quantity=" + quantity +
                ", unit='" + unit + '\'' +
                ", total=" + total +
                '}';
    }
}
