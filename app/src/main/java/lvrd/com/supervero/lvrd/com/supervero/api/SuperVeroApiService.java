package lvrd.com.supervero.lvrd.com.supervero.api;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import io.realm.RealmResults;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.Pay;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductPagination;
import lvrd.com.supervero.lvrd.com.supervero.model.RegisterPayResponse;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.model.Sale;
import lvrd.com.supervero.lvrd.com.supervero.model.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SuperVeroApiService {

    /**
     * Autenticacion del usuario
     * @param username
     * @param password
     * @return
     */
    @POST("/api/auth/signin")
    Call<User> signin(@Query("username") String username, @Query("password") String password);

    /**
     * Obtener todos los productos (No recomendable de usar)
     * @return
     */
    @GET("api/products")
    Call<ArrayList<Product>> getProducts();

    /**
     * Obtener productos por paginacion (Recomendado para no cargar la aplicacion)
     * @param limit
     * @param offSet
     * @return
     */
    @GET("api/products/pages/{offset}/{limit}")
    Call<ProductPagination> getProductsPagination(@Path("offset") int offSet, @Path("limit") int limit);

    /**
     * Obtener todos los clientes
     * @return
     */
    @GET("/api/customers")
    Call<ArrayList<Customer>> getCustomer();

    /**
     * Obtener los clientes por ruta
     */
    @GET("/api/getCustomerOnlyRoute/{route}")
    Call<ArrayList<Customer>> getCustomerByRoute(@Path("route") String route);

    /**
     * Obtener todos los pagos(No se utiliza)
     * @param pay
     * @return
     */
    @POST("/api/payments/")
    Call<RegisterPayResponse> doPay(@Body Pay pay);
    //Call<ResponseBody>doPay(@Body Pay pay);

    /**
     * Obtener todos los pedidos ordenados por fecha  y solo con estatus  "pendientes"
     * @param
     * @return
     */
    @GET("/api/sales/")
    Call<ArrayList<SaleLoaded>> getSalesLoaded();

    /**
     * Obtener todos los pedidos ordenanos por ruta
     */
    @GET("/api/getSalesOnlyRoute/{route}")
    Call<ArrayList<SaleLoaded>> getSalesLoadedByRoute(@Path("route") String route);

    /**
     * Cargar pedido
     * @param sale
     * @return
     */
    @POST("/api/sales")
    Call<SaleLoaded> doSale(@Body Sale sale);

    /**
     * Buscar producto por palabra
     */
    @GET("api/products/find-name/{nameProduct}")
    Call<ArrayList<Product>> searchProducts(@Path("nameProduct") String nameProduct);

    /**
     * Cambiar el estatus del pedido a Entregado
     * @param id
     * @return
     */
    @POST("api/dispatchedSale/{id}")
    Call<JsonObject> changeStatusSale(@Path("id") int id);

    /**
     * Obtener el estado de cuenta de un cliente
     * @param id
     * @return ArrayList de estado de cuenta
     */
    @GET("api/customerBalance/{id}")
    Call<AccountStateCustomer> getCustomerAccount(@Path("id") int id);
}
