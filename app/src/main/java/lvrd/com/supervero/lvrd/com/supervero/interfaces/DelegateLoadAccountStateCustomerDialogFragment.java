package lvrd.com.supervero.lvrd.com.supervero.interfaces;

import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;

public interface DelegateLoadAccountStateCustomerDialogFragment {

    /**
     * Cargar estado de cuenta de un cliente
     */
    public abstract void loadAccountStateCustomer(AccountStateCustomer accountStateCustomer);
}
