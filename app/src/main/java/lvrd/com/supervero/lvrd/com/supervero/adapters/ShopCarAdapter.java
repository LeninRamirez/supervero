package lvrd.com.supervero.lvrd.com.supervero.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductAddedShopCar;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class ShopCarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    // Layout para mostrar item
    private static final int ITEM = 0;

    // Layout para cuando se encuentra vacio la lista del carrito
    private static final int EMPTY = 1;

    // Lista de productos al carrito
    private ArrayList<ProductAddedShopCar> shopCar = new ArrayList<ProductAddedShopCar>();

    // Layout para mostrar item
    private int layout = 0;

    // Contexto de la aplicacion
    private Context context = null;
    private DelegateMainActivity delegateMainActivity;

    // Escuchador
    private OnItemClickListener listener;

    /**
     *
     * @param shopCar
     * @param context
     */
    public ShopCarAdapter(ArrayList<ProductAddedShopCar> shopCar, Context context,DelegateMainActivity delegateMainActivity,int layout,
                          OnItemClickListener listener){//DelegateMainActivity delegateMainActivity) {
        this.shopCar = shopCar;
        this.context = context;
        this.delegateMainActivity = delegateMainActivity;
        this.layout = layout;
        this.listener = listener;
    }

    /**
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(viewGroup, inflater);
                break;
            case EMPTY:
                View view = inflater.inflate(R.layout.empty, viewGroup, false);
                viewHolder = new EmptyViewHolder(view);
                break;
        }
        return viewHolder;

        /*int layout = viewType == 0 ? R.layout.empty:R.layout.layout_cardview_shop_car;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout,viewGroup,false);

        RecyclerView.ViewHolder shopCarViewHolder = null;
        if (viewType == R.layout.empty) {
            shopCarViewHolder = new EmptyViewHolder(view);
        }else{
            shopCarViewHolder = new ShopCarViewHolder(view);
        }

        return shopCarViewHolder;*/

    }

    /**
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        /*if (shopCarViewHolder instanceof EmptyViewHolder){

        }else{
            ShopCarViewHolder viewHolder = (ShopCarViewHolder) shopCarViewHolder;
            // Asignar nombre
            viewHolder.nameTv.setText(shopCar.get(i).getName());

            NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
            String sellPriceString = format.format(shopCar.get(i).getSellPrice());
            viewHolder.sellPriceTv.setText(sellPriceString);

            viewHolder.quantityTv.setText(String.valueOf(shopCar.get(i).getQuantity()));

            String totalString = format.format(shopCar.get(i).getTotal());
            viewHolder.totalTv.setText(totalString);
        }*/

        switch (getItemViewType(position)) {
            case ITEM:
                final ShopCarViewHolder shopCarViewHolder = (ShopCarViewHolder) viewHolder;
                shopCarViewHolder.bind(shopCar.get(position),listener);
                break;
            case EMPTY:

                break;
        }
    }



    /**
     * Obtener el total de la lista de productos agregados al carrito de compras
     * @return int
     */
    @Override
    public int getItemCount() {
        if(this.shopCar.size() == 0){
            return 1;
        }
        return shopCar.size();
    }

    /**
     * Obtener el layout que se mostrara, en caso de no tener productos agregados
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return (this.shopCar.size() == 0) ? EMPTY : ITEM;
        //return (position == this.shopCar.size() - 1 ) ? EMPTY : ITEM;
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View view = inflater.inflate(layout, parent, false);
        viewHolder = new ShopCarViewHolder(view);
        return viewHolder;
    }

    /*
        ---------------------------  View Holder -------------------------------
     */

    /**
     * Clase interna patron View Holder para mostrar los productos en el carrito
     */
    public class ShopCarViewHolder extends RecyclerView.ViewHolder{

        // Componentes de interfaz

        @BindView(R.id.tv_product_name_shop_car)
        TextView nameTv;

        @BindView(R.id.tv_product_sell_price_shop_car)
        TextView sellPriceTv;

        @BindView(R.id.tv_product_quantity_shop_car)
        TextView quantityTv;

        @BindView(R.id.tv_product_total_shop_car)
        TextView totalTv;

        @BindView(R.id.ib_product_remove_shop_car)
        ImageButton removeProductIb;

        @BindView(R.id.ib_product_update_shop_car)
        ImageButton updateProductIb;

        /**
         * Constructor
         * @param view
         *
         */
        public ShopCarViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(ProductAddedShopCar productAddedShopCar, OnItemClickListener listener) {

            this.nameTv.setText(productAddedShopCar.getName());
            this.sellPriceTv.setText(Utils.formatMoney(productAddedShopCar.getSellPrice()));
            this.quantityTv.setText(productAddedShopCar.getQuantity()+"");
            this.totalTv.setText(Utils.formatMoney(productAddedShopCar.getTotal()));

            // Evento sobre el boton agregar producto del CardView
            removeProductIb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ProductAddedShopCar productAddedShopCar = shopCar.get(position);
                    createConfirmationDialog(productAddedShopCar);
                }
            });

            // Evento sobre el boton actualizar cantidad del producto a llevar
            updateProductIb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ProductAddedShopCar productAddedShopCar = shopCar.get(position);
                    delegateMainActivity.updateQuantityProductShopCar(productAddedShopCar);

                }
            });

        }

        /**
         * Crear cuadro de dialogo para confirmar la eliminicacion del producto en el carro de de compras
         *
         */
        private void createConfirmationDialog(ProductAddedShopCar productAddedShopCar){
            // create a dialog with AlertDialog builder
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Theme_AppCompat_Dialog_Alert);
            builder.setTitle(context.getString(R.string.title_confirm));
            builder.setMessage("¿Desea eliminar el producto "+productAddedShopCar.getName()+"?");

            builder.setPositiveButton(context.getString(R.string.msg_positive_confirm),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            delegateMainActivity.removeProductShopCar(productAddedShopCar);
                            shopCar.remove(productAddedShopCar);
                            notifyDataSetChanged();
                            dialog.dismiss();

                        }
                    });

            builder.setNegativeButton(context.getString(R.string.msg_negative_confirm),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    /**
     * Clase interna patro View Holder para mostrar vacio
     */
    public class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    /*
        Interface de comunicacion
     */

    /**
     * Interface de comunicacion entre el adaptador y fragmento
     **/
    public interface OnItemClickListener {
        void onItemClick(ProductAddedShopCar productAddedShopCar, int position);
    }
}
