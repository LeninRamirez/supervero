package lvrd.com.supervero.lvrd.com.supervero.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.fragments.LoadPayDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.model.Sale;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.utils.SaleStatus;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class SaleLoadedAdapter extends RecyclerView.Adapter<SaleLoadedAdapter.ViewHolder> {

    // Datos de los pedidos cargados en el sistema
    private ArrayList<SaleLoaded> salesLoaded;

    // Layout Item para cada pedido cargado en el sistema
    private int layout;

    // Contexto del fragment
    Context context;

    // Instancia de interface de comunicacion
    private OnItemClickListener onItemClickListener;

    private OnMenuItemClickListener onMenuItemClickListener;

    /**
     * Constructor
     * @param salesLoaded
     * @param layout
     * @param context
     * @param onItemClickListener
     * @param onMenuItemClickListener
     */
    public SaleLoadedAdapter(ArrayList<SaleLoaded> salesLoaded, int layout, Context context, OnItemClickListener onItemClickListener, OnMenuItemClickListener onMenuItemClickListener) {
        this.salesLoaded = salesLoaded;
        this.layout = layout;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
        this.onMenuItemClickListener = onMenuItemClickListener;
    }

    public void loadSalesLoaded(ArrayList<SaleLoaded> salesLoaded){
        this.salesLoaded.clear();
        this.salesLoaded = salesLoaded;
        notifyDataSetChanged();
    }


    @Override
    public SaleLoadedAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // Cargar layout
        View view = LayoutInflater.from(context).inflate(layout,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SaleLoadedAdapter.ViewHolder viewHolder, int i) {
        // Asociar el pedido cargado con el escuchador
        viewHolder.bind(salesLoaded.get(i), onItemClickListener,onMenuItemClickListener);
    }

    @Override
    public int getItemCount() {
        return salesLoaded.size();
    }


    public class ViewHolder extends  RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, PopupMenu.OnMenuItemClickListener {

        @BindView(R.id.textViewAmountTotal_recycle_view_sale_loaded_item)
        TextView textViewAmountTotal;

        @BindView(R.id.textViewCustomerName_recycle_view_sale_loaded_item)
        TextView textViewCustomerName;

        @BindView(R.id.textViewPendingToPay_recycle_view_sale_loaded_item)
        TextView textViewPendingToPay;

        @BindView(R.id.textViewSaleId_recycle_view_sale_loaded_item)
        TextView textViewSaleId;

        @BindView(R.id.textViewStatusSale_recycle_view_sale_loaded_item)
        TextView textViewStatusSale;

        @BindView(R.id.textViewDeliverDate_recycle_view_sale_loaded_item)
        TextView textViewDeliverDate;

        @BindView(R.id.textViewRealDate_recycle_view_sale_loaded_item)
        TextView textViewRealDate;

        @BindView(R.id.textViewDeliverPlanDate_recycle_view_sale_loaded_item)
        TextView textViewDeliverPlanDate;


        public ViewHolder(View itemView) {
            super(itemView);
            // Cargar ButterKnife
            ButterKnife.bind(this,itemView);

            // Añadimos al view el listener para el context menu, en vez de hacerlo en
            // el activity mediante el método registerForContextMenu
            itemView.setOnCreateContextMenuListener(this);
        }

        public void bind(SaleLoaded saleLoaded,OnItemClickListener onItemClickListener,OnMenuItemClickListener onMenuItemClickListener){

            textViewAmountTotal.setText(Utils.formatMoney(saleLoaded.getTotalAmount())+"");
            textViewCustomerName.setText(context.getString(R.string.label_textview_customer_name)+saleLoaded.getCustomer().getSocialName());
            textViewPendingToPay.setText(Utils.formatMoney(saleLoaded.getTotalLeft())+"");
            textViewSaleId.setText(context.getString(R.string.label_textview_sale_id)+saleLoaded.getId()+"");
            if(saleLoaded.getStatus().equals(SaleStatus.PAGADO.getStatus())){
                textViewStatusSale.setTextColor(context.getResources().getColor(R.color.primary_dark));
            }else if(saleLoaded.getStatus().equals(SaleStatus.PENDIENTE.getStatus())){
                textViewStatusSale.setTextColor(Color.RED);
            }else if(saleLoaded.getStatus().equals(SaleStatus.CANCELADO.getStatus())){
                textViewStatusSale.setTextColor(Color.YELLOW);
            }
            textViewStatusSale.setText(context.getString(R.string.label_textview_sale_status)+saleLoaded.getStatus());

            textViewDeliverDate.setText(Utils.formatDate(saleLoaded.getDeliverPlanDate()));
            textViewRealDate.setText(Utils.formatDate(saleLoaded.getRealDate()));
            textViewDeliverPlanDate.setText(Utils.formatDate(saleLoaded.getDeliverDate()));

            // Añadimos el listener click para cada elemento fruta
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(saleLoaded, getAdapterPosition());
                }
            });
        }



        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            PopupMenu popup = new PopupMenu(v.getContext(), v);
            popup.getMenuInflater().inflate(R.menu.menu_sales_loaded, popup.getMenu());
            popup.setOnMenuItemClickListener(this);
            popup.show();
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                // Cargar pago
                /*case R.id.menu_sale_loaded_load_pay:
                    onMenuItemClickListener.onMenuItemClick(salesLoaded.get(this.getAdapterPosition()),getAdapterPosition(), MainActivity.MENU_LOAD_PAY);
                return true;*/

                // Ver los productos del pedido
                case R.id.menu_sale_loaded_load_products:
                    onMenuItemClickListener.onMenuItemClick(salesLoaded.get(this.getAdapterPosition()),getAdapterPosition(),MainActivity.MENU_SEE_PRODUCTS);
                    return true;

                // Imprimir comprobante del ultimo pago realizado
                case R.id.menu_sale_loaded_printer_pay:
                    onMenuItemClickListener.onMenuItemClick(salesLoaded.get(this.getAdapterPosition()),getAdapterPosition(),MainActivity.MENU_PRINTER_PAY);
                    return true;

                // Imprimir comprobante de la entrega del pedido
                case R.id.menu_sale_loaded_delivery_sale:
                    onMenuItemClickListener.onMenuItemClick(salesLoaded.get(this.getAdapterPosition()),getAdapterPosition(),MainActivity.MENU_DELIVERY_SALE);
                    return true;

                default:
                    return false;
            }
        }
    }


    /**
     * Interface de comunicacion
     */
    public interface OnItemClickListener{
        void onItemClick(SaleLoaded saleLoaded,int position);
    }

    /**
     * Interface de comunicacion
     */
    public interface OnMenuItemClickListener{
        void onMenuItemClick(SaleLoaded saleLoaded,int position,int opcionMenu);
    }
}
