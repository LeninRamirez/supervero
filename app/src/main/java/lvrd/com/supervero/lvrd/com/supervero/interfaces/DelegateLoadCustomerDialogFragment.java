package lvrd.com.supervero.lvrd.com.supervero.interfaces;

import java.util.ArrayList;

import lvrd.com.supervero.lvrd.com.supervero.model.Customer;

public interface DelegateLoadCustomerDialogFragment {
    public void loadCustomers(ArrayList<Customer> response);
}
