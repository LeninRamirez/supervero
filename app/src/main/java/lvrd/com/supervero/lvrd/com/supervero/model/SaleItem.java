package lvrd.com.supervero.lvrd.com.supervero.model;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lvrd.com.supervero.lvrd.com.supervero.app.SuperVeroApp;

public class SaleItem extends RealmObject implements Serializable {

    //@PrimaryKey
    //private long id;
    private int ProductId;
    private String Name;
    private float SellPrice;
    private String Unit;
    private int Quantity;
    private float Subtotal;


    public SaleItem() {

    }

    public SaleItem(int productId, String name, float sellPrice, String unit, int quantity) {
        ProductId = productId;
        Name = name;
        SellPrice = sellPrice;
        Unit = unit;
        Quantity = quantity;
    }

    /*public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }*/

    public int getProductId() {
        return ProductId;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public float getSellPrice() {
        return SellPrice;
    }

    public void setSellPrice(float sellPrice) {
        SellPrice = sellPrice;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public float getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(float subtotal) {
        Subtotal = subtotal;
    }

    @Override
    public String toString() {
        return "SaleItem{" +
                "ProductId=" + ProductId +
                ", Name='" + Name + '\'' +
                ", SellPrice=" + SellPrice +
                ", Unit='" + Unit + '\'' +
                ", Quantity=" + Quantity +
                ", Subtotal=" + Subtotal +
                '}';
    }
}
