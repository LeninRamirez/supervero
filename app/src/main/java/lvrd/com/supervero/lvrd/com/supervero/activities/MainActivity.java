package lvrd.com.supervero.lvrd.com.supervero.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import lvrd.com.supervero.lvrd.com.supervero.app.SuperVeroApp;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadCustomerAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadCustomerToDBAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadPayAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadSaleAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadAccountStateCustomerToDBAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadSearchProductAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.fragments.AddProductDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.fragments.LoadPayDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.fragments.PagerAdapter;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadProductAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.fragments.PayFragment;
import lvrd.com.supervero.lvrd.com.supervero.fragments.SaleLoadedDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.fragments.SelectedCustomerForSeeAccountStateDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.Pay;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.fragments.ProductFragment;
import lvrd.com.supervero.lvrd.com.supervero.fragments.ShopCarFragment;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductAddedShopCar;
import lvrd.com.supervero.lvrd.com.supervero.model.RegisterPayResponse;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.model.Sale;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleItem;
import lvrd.com.supervero.lvrd.com.supervero.utils.Printer;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements DelegateMainActivity,
        SearchView.OnQueryTextListener, AddProductDialogFragment.DelegateActivityDialogFragment,LoadPayDialogFragment.DelegateLoadPay,
        RealmChangeListener<RealmResults<Product>> {

    // Constantes para opciones de pedidos
    public static final int MENU_LOAD_PAY = 0;
    public static final int MENU_SEE_PRODUCTS = 1;
    public static final int MENU_PRINTER_PAY = 2;
    public static final int MENU_DELIVERY_SALE = 3;

    //Código de envío entre actividades
    public final static int REQUEST_CODE = 2;

    // Contexto
    private Context context = null;

    // Almacenanimiento
    private SharedPreferences  appSharedPreferences;

    // Editor del almacenamiento
    private SharedPreferences.Editor appSharedPreferencesEditor;

    //
    private ArrayList<Product> shopCar;

    // Array para almacenar los 3 fragmentos
    private ArrayList<Fragment> fragments;

    // Fragmento Productos
    private ProductFragment productFragment = null;

    // Fragmento Carrito de compras
    private ShopCarFragment shopCarFragment = null;

    // Fragmento de Pago
    private PayFragment payFragment = null;

    // Fragmento de Pedidos Cargados
    //private SaleLoadedDialogFragment saleLoadedDialogFragment = null;

    // Comunicador entre 'MainActivity' y 'LoadProductAsyncTask'
    private DelegateMainActivity delegateMainActivity = null;

    // Tarea asincrona para realizar la carga de productos
    private LoadProductAsyncTask mLoadProductsTask = null;

    // Tarea asincrona para realizar la carga de clientes
    private LoadCustomerAsyncTask mLoadCustomerAsyncTask = null;

    // Tarea asincrona para obtener los pedidos cargados
    //private LoadSaleLoadedAsyncTask mLoadSaleLoadedAsyncTask;

    // Tarea asincrona para buscar productos
    private LoadSearchProductAsyncTask mLoadSearchProductAsyncTask = null;

    // Iconos de los tabs
    private int[] tabIcons = {
            R.drawable.ic_shop_white_24dp,
            R.drawable.ic_shopping_cart_white_24dp,
            R.drawable.ic_attach_money_white_24dp
            //R.drawable.ic_playlist_add_check_white_24dp
    };

    // Menu para buscar/filtrar productos
    private MenuItem searchItem;

    // Menu para poner modo offline/online
    private MenuItem modeOffLineItem;

    // Menu para ver los pedidos
    private MenuItem seeSalesLoaded;

    // Menu para obtener todos los productos
    private MenuItem synchronizeProductsItem;

    // Menu sincronizar ventas y pagos
    private MenuItem synchronizeSaleAndPay;

    // Instancia de la base de datos
    private Realm realm;

    // Arreglo para obtener los productos desde la base de datos
    private RealmResults<Product> productsBD;

    // Lista de productos
    ArrayList<Product> products = new ArrayList<Product>();

    // Lista de productos agregados al carrito
    ArrayList<Product> productsAddedShopCar = new ArrayList<Product>();

    // Cuadro de progreso cuando se sincronizan productos,ventas y clientes
    private Dialog mProgressDialogForModeOffLine;

    // Total de sincronizacion para pasar las ventas almacenados en BD a la nube
    private int countSales = 0;

    // Total de estasdos de cuenta para determinar las veces que se debe llamar el ws
    private int countAccountStateCustomer = 0;

    // ID del vendedor
    public static String id;

    // Ruta del vendedor
    public static  String route;

    // Nombre del vendedor
    public static String userName;

    // Modo de la app
    public static boolean mode_offline_app;

    // Objeto para imprimir tickets
    private Printer printer = null;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.pager)
    ViewPager viewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    Product productPreSelected;

    // Cliente seleccionado
    Customer customerSelected = null;

    // Respuesta del pedido cargado al sistema
    SaleLoaded saleLoaded = null;

    // Estado de cuenta del cliente seleccionado != Cliente seleccionado para el flujo principal de la venta
    AccountStateCustomer accountStateCustomer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Asociar layout
        setContentView(R.layout.activity_main);

        // Envolver los componentes del layout
        ButterKnife.bind(this);

        // Asociar contexto de la actividad
        context = this;


        // Iniciar el comunicador
        delegateMainActivity = this;

        Realm.init(this);

        try{
            realm = Realm.getDefaultInstance();

        }catch (Exception e){

            // Get a Realm instance for this thread
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(config);

        }
        // Obtener la instancia de la base de datos Realm
        //realm = Realm.getDefaultInstance();

        // Configurar almacenamiento (SharedPreferences)
        configureShareprefrecens();

        // Obtener la bandera para saber si la app esta en modo offline
        mode_offline_app = appSharedPreferences.getBoolean(getString(R.string.PREF_MODE_APP),false);

        // Iniciar fragmentos
        initFragments();

        // Configurar dialogo de progreso para  la sincronizacion del los datos en modo fuera de linea
        setupDialogProgressForModeOffLine();

        //Obteniendo la instancia del Intent LoginActivity
        Intent intent = getIntent();


        //Extrayendo el extra Id del usuario
        id = intent.getStringExtra(getString(R.string.PREF_USER_ID));

        // Extrayedo el extra nombre del usuario
        userName = intent.getStringExtra(getString(R.string.PREF_USER_NAME));

        // Crear instancia para mandar a imprimir
        printer = new Printer(this);

        // Extrayendo la ruta por correspondiente del usuario, para consultar clientes
        route = intent.getStringExtra(getString(R.string.PREF_USER_ROUTE));

        // Registrando datos en el almacenamiento(SharedPreferences)
        appSharedPreferencesEditor.putString(getString(R.string.PREF_USER_ID),id);
        appSharedPreferencesEditor.putString(getString(R.string.PREF_USER_NAME),userName);
        appSharedPreferencesEditor.putString(getString(R.string.PREF_USER_ROUTE),route);
        appSharedPreferencesEditor.commit();

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(mode_offline_app){
            getSupportActionBar().setLogo(R.drawable.ic_cloud_off_white_24dp);

        }else{
            getSupportActionBar().setLogo(R.drawable.ic_cloud_white_24dp);
        }

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(),context,fragments);
        //ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);

        // Limitar el numero de tabs y guardar sus estados
        viewPager.setOffscreenPageLimit(2);

        //TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        //tabLayout.getTabAt(3).setIcon(tabIcons[3]);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
              if(position>0){
                    searchItem.setVisible(false);
                    searchItem.collapseActionView();
                    if(position==2){
                        if(shopCarFragment.getShopCar().size() == 0){
                            Toasty.error(context,getString(R.string.error_msg_empty_products)).show();
                            viewPager.setCurrentItem(0,true);
                            return;
                        }
                        if(customerSelected == null){
                            Toasty.error(context,getString(R.string.erro_msg_not_selected_customer)).show();
                            viewPager.setCurrentItem(1,true);
                            return;
                        }
                    }
                  // Validar que sea el fragmen 2 "pagar"
                  if(position != 2) {
                      // Validar si se cargo un pedido en el sistema
                      if (saleLoaded != null) {
                          // Llamar cuadro de dialogo
                          confirmClearProcess();
                          return;
                      }
                  }
              }else{
                  searchItem.setVisible(true);

                  // Validar que sea el fragmen 2 "pagar"
                  if(position != 2) {
                      // Validar si se cargo un pedido en el sistema
                      if (saleLoaded != null) {
                          // Llamar cuadro de dialogo
                          confirmClearProcess();
                          return;
                      }
                  }
              }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //requestSalesLoaded();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        route = appSharedPreferences.getString(getString(R.string.PREF_USER_ROUTE),"");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Carga Menu
     * @params menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        /*MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);*/
        int currentTab = viewPager.getCurrentItem();

        // Obtener el menu modo offline/online
        modeOffLineItem = menu.findItem(R.id.action_mode_offline);

        // Obtener el menu ver pedidos
        seeSalesLoaded = menu.findItem(R.id.action_see_sales);

        // Obtener el menu sincronizar todos los productos
        synchronizeProductsItem = menu.findItem(R.id.action_update_data_base);

        // Obtener el menu sincronizar las ventas y pagos
        synchronizeSaleAndPay = menu.findItem(R.id.action_synchronized_sale_and_pay);

        // Verificar si existen ventas o pagos pendientes por sincronizar
        if(existsSaleOrPayForSyncUp()){
            synchronizeSaleAndPay.setVisible(true);
        }else{
            synchronizeSaleAndPay.setVisible(false);
        }

        // Obtener el menu para realizar busqueda de productos
        searchItem = menu.findItem(R.id.action_search);


        // Asignar el titulo del menu segun en el modo que se encuentre offline o online
        if(mode_offline_app){
            modeOffLineItem.setTitle(getString(R.string.action_mode_online));
            synchronizeProductsItem.setVisible(true);// mostrar opcion en menu para sincronizar con la base de datos
            seeSalesLoaded.setVisible(false);
            this.getAllProductsFromDataBase();
        }else{
            modeOffLineItem.setTitle(getString(R.string.action_mode_offline));
            synchronizeProductsItem.setVisible(false); // ocultar opcion en menu para sincronizar con la base de datos
        }

        SearchView searchView = (SearchView) searchItem.getActionView();//MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        /*return super.onCreateOptionsMenu(menu);*/
        return true;
    }

    /**
     * Seleccion de opcion en el menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Opcion seleccionada
        switch (item.getItemId()) {
            case R.id.action_logout:
                // Salir de la actividad e ir al LoginActivity
                logout();
                return true;
            case R.id.action_see_sales:
                // Ver los pedidos
                seeSaleLoaded();
                return true;
            case R.id.action_see_customers:
                // Ver los clientes
                seeCustomers();
                return true;
            case R.id.action_update_data_base:
                // Descargar todos los productos al movil
                downloadDataBase();
                return true;
            case R.id.action_synchronized_sale_and_pay:
                // sincronizar ventas y pagos
                syncUpSales();
                return true;
            case R.id.action_mode_offline:
                // Cambiar titulo de la opcion para reutilizar el mismo item
                if(item.getTitle().equals(getString(R.string.action_mode_offline))){
                    modeOffLineItem.setTitle(getString(R.string.action_mode_online)); // Cambiar el texto de menu item
                    setModeOffLine(true); // Cambiar la bandera del enviroment y almacenar en shared preferences
                    getSupportActionBar().setLogo(R.drawable.ic_cloud_off_white_24dp);
                    mode_offline_app = true;
                    productFragment.getProductAdapter().getProducts().clear();
                    productFragment.getProductAdapter().getProductsFiltered().clear();
                    productFragment.getProductAdapter().notifyDataSetChanged();
                    this.getAllProductsFromDataBase(); // Obtener todos los registros desde la base de datos
                    seeSalesLoaded.setVisible(false);
                }else{
                    seeSalesLoaded.setVisible(true);
                    modeOffLineItem.setTitle(getString(R.string.action_mode_offline)); // Fuera de linea
                    setModeOffLine(false);

                    getSupportActionBar().setLogo(R.drawable.ic_cloud_white_24dp);
                    mode_offline_app = false;
                    productFragment.getProductAdapter().getProducts().clear();
                    productFragment.getProductAdapter().getProductsFiltered().clear();
                    productFragment.getProductAdapter().notifyDataSetChanged();
                    this.getProductsFromServer();
                    //productFragment.loadFirstPage();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Inicializar el event bus y carro de venta
     */
    private void initFragments() {
        // Inicializar array del carro de ventas
        shopCar = new ArrayList<Product>();

        // Crear fragmentos
        productFragment = new ProductFragment();
        shopCarFragment = new ShopCarFragment();
        payFragment = new PayFragment();

        //saleLoadedDialogFragment = new SaleLoadedDialogFragment();

        fragments = new ArrayList<Fragment>();

        fragments.add(0,productFragment);

        fragments.add(1,shopCarFragment);

        fragments.add(2,payFragment);

        //fragments.add(3, saleLoadedDialogFragment);
    }

    /**
     * Ver todos los pedidos
     */
    private void seeSaleLoaded(){
        SaleLoadedDialogFragment saleLoadedDialogFragment = SaleLoadedDialogFragment.newInstance();
        saleLoadedDialogFragment.show(this.getSupportFragmentManager(), "saleLoadedDialogFragment");
    }

    /**
     * Ver la lista de clientes
     */
    private void seeCustomers(){
        SelectedCustomerForSeeAccountStateDialogFragment selectedCustomerForSeeAccountStateDialogFragment = SelectedCustomerForSeeAccountStateDialogFragment.newInstance();
        selectedCustomerForSeeAccountStateDialogFragment.show(getSupportFragmentManager(),"CustomersForAccountState");
    }

    private void logout(){
        // Contruccion de cuadro de confirmacion
        AlertDialog.Builder logoutConfirmAlertDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialogCustom));
        logoutConfirmAlertDialog.setTitle(getString(R.string.title_confirm));
        logoutConfirmAlertDialog.setMessage(getString(R.string.msg_logout_confirm));
        logoutConfirmAlertDialog.setCancelable(false);

        // Evento para el boton positivo
        logoutConfirmAlertDialog.setPositiveButton(getString(R.string.msg_positive_confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {

                appSharedPreferencesEditor.remove(getString(R.string.PREF_USER_NAME)).commit();
                appSharedPreferencesEditor.remove(getString(R.string.PREF_USER_ID)).commit();
                //Terminar la actividad
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                //Pasaando a la actividad "MainActivity"
                startActivityForResult(intent,REQUEST_CODE);

                // Animacion de finalizacion
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }
        });

        // Evento paral boton negativo
        logoutConfirmAlertDialog.setNegativeButton(getString(R.string.msg_negative_confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                dialogo1.dismiss();
            }
        });

        // Mostrar cuadro de confirmacion
        logoutConfirmAlertDialog.show();

    }

    @Override
    public boolean onQueryTextSubmit(String s) {

        // Nombre del producto a buscar
        String nameProduct = s.toUpperCase();

        productFragment.showProgressBar(true);

        if(!mode_offline_app) {
            //productFragment.setSearch(true);
            if (mLoadSearchProductAsyncTask == null) {
                mLoadSearchProductAsyncTask = new LoadSearchProductAsyncTask(delegateMainActivity, nameProduct);
                mLoadSearchProductAsyncTask.execute();

            }
        }else{
            if(nameProduct != "") {
                productFragment.getProductAdapter().filter(nameProduct);
            }
        }
        //productFragment.getProductAdapter().getFilter().filter(s);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (TextUtils.isEmpty(s)) {

            if(!mode_offline_app) {
                ArrayList<Product> products = new ArrayList<Product>();
                //productFragment.setSearch(false);
                productFragment.getProductAdapter().updateData(products);
                productFragment.loadFirstPage();
            }else{
                productFragment.getProductAdapter().filter(s);
            }

        }


        /*String productInput = s.toLowerCase();
        ArrayList<Product> productsFilter = new ArrayList<Product>();

        for (Product product : products){
            if(product.getName().toLowerCase().contains(productInput)){
                productsFilter.add(product);
            }
        }*/

        //productFragment.getProductAdapter().getFilter().filter(s);
        return false;
    }



    //@Override
    //public void responseProducts(Response<ArrayList<Product>> response) {
        //products = response.body();

        //products.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        //productFragment.getProductAdapter().updateData(products);
        //////productFragment.setAllProducts(products);
    //}

    //@Override
    //public void responseCustomers(Response<ArrayList<Customer>> response) {
        //shopCarFragment.showCustomers();
    //}



    @Override
    public void selectedProduct(Product product) {
        // Agregar producto al carro de venta
        productPreSelected = product;

        // Mensaje de notificacion "Producto agregado"
        //Message.successMessage(context,context.getString(R.string.added_product));
        productFragment.openDialog(product);
    }

    private void configureShareprefrecens() {
        appSharedPreferences = getSharedPreferences(getString(R.string.PREF_APP),Context.MODE_PRIVATE);
        appSharedPreferencesEditor =appSharedPreferences.edit();
    }

    @Override
    public void quiantityProducts(int count) {
        ProductAddedShopCar productAddedShopCar = new ProductAddedShopCar(productPreSelected.getId(),
                productPreSelected.getName(),productPreSelected.getSellPrice(),count,productPreSelected.getUnit());
        shopCarFragment.addProduct(productAddedShopCar);
        productsAddedShopCar.add(productPreSelected);
        int position = getIndexProducts(productPreSelected);
        productFragment.getProductAdapter().getProducts().remove(position);
        productFragment.getProductAdapter().getProductsFiltered().remove(position);
        productFragment.getProductAdapter().notifyDataSetChanged();
        //Message.successMessage(context,context.getString(R.string.added_product));
        Toasty.success(context,getString(R.string.added_product)).show();
        float total = calculateTotalOrder();
        payFragment.setTotalOrder(total);

    }

    @Override
    public void removeProductShopCar(ProductAddedShopCar productAddedShopCar) {
        Toasty.warning(context,getString(R.string.canceled_product)).show();
        for (Product product : productsAddedShopCar){//productFragment.getProductAdapter().getProductsOfSaleLoaded()) {
            if (product.getId() == productAddedShopCar.getId()) {
                productFragment.getProductAdapter().getProducts().add(product);
                productFragment.getProductAdapter().getProductsFiltered().add(product);
                productFragment.getProductAdapter().notifyDataSetChanged();
                break;
            }
        }
    }

    @Override
    public void updateQuantityProductShopCar(ProductAddedShopCar productAddedShopCar) {
        for (Product product : productsAddedShopCar){//productFragment.getProductAdapter().getProductsOfSaleLoaded()) {
            if (product.getId() == productAddedShopCar.getId()) {
                // Agregar producto al carro de venta
                productPreSelected = product;

                // Mensaje de notificacion "Producto agregado"
                //Message.successMessage(context,context.getString(R.string.added_product));
                productFragment.openDialog(product);
                break;
            }
        }
    }

    @Override
    public void selectedCustomer(Customer customer) {
        customerSelected = customer;
        toolbar.setSubtitle("");
        toolbar.setSubtitle("Cliente : "+customerSelected.getSocialName());
        // Mostrar mensaje seleccion de cliente
        Toasty.info(this, getString(R.string.msg_notification_selected_customer), Toast.LENGTH_LONG, true).show();
        payFragment.setProductAddedShopCars(shopCarFragment.getShopCar());
    }

    @Override
    public void loadSale(String deliverDate) {
        // Limpiar el anterior pedido cargado al sistema
        saleLoaded = null;

        if(mode_offline_app) {
            realm.beginTransaction();
        }

        Sale sale = new Sale(customerSelected.getId().intValue(),calculateTotalOrder(),currentDate(),deliverDate,Integer.parseInt(id));
        RealmList<SaleItem> saleItems = new RealmList<SaleItem>();

        ArrayList<ProductAddedShopCar> shopCar = shopCarFragment.getShopCar();
        for (ProductAddedShopCar productAddedShopCar : shopCar){
            SaleItem saleItem = new SaleItem(productAddedShopCar.getId(),productAddedShopCar.getName(),
                    productAddedShopCar.getSellPrice(),productAddedShopCar.getUnit(),productAddedShopCar.getQuantity());
            saleItem.setSubtotal(productAddedShopCar.getTotal());
            //saleItem.setId(getPrimaryKeySaleItemClass());
            saleItems.add(saleItem);
        }
        sale.setItems(saleItems);

        // Validar si la app se encuentra en modo off line
        if(mode_offline_app) {
            // Guardar la venta en la base de datos
            sale.setId(new Long(getPrimaryKeySaleClass()));
            realm.copyToRealm(sale);

            payFragment.closeDialogProgress();
            // Llamar para ver si existe algun anticipo(pago) y guardar el pago en la base de datos
            savePayToBD(sale.getId().intValue());
        }else{
            LoadSaleAsyncTask loadSaleAsyncTask = new LoadSaleAsyncTask(delegateMainActivity, sale);
            loadSaleAsyncTask.execute();
        }
    }

    @Override
    public void responseSale(Response<SaleLoaded> registerSaleResponse,boolean isLocal) {

        if(registerSaleResponse.isSuccessful()) {
            Toasty.success(context, getString(R.string.msg_load_sale_success)).show();

            // Checar en caso de que sea guardado local
            if(isLocal){
                responseSaleWithOffLineAsyncTask(registerSaleResponse);
                return;
            }
            // Guardar la respuesta del servidor al cargar un pedido
            saleLoaded = registerSaleResponse.body();

            try {
                // Obtener el pago de aportacion al pedido
                float importPay = Float.parseFloat(payFragment.getEtPayQuantityToPay().getText().toString());

                // Crear objeto Pago
                Pay pay = new Pay();
                //((PayWithSale) pay).setSaleId(registerSaleResponse.body().getId());
                pay.setCustomerId(registerSaleResponse.body().getCustomerId());
                pay.setSubtotal(importPay);
                pay.setCreatedById(Integer.parseInt(id));

                // Llamar asynctask para realizar pago
                LoadPayAsyncTask loadPayAsyncTask = new LoadPayAsyncTask(delegateMainActivity,pay,false);
                loadPayAsyncTask.execute();

                // Recargar pedidos
                //requestSalesLoaded();

            }catch(NumberFormatException err){
                // Ocultar teclado del telefono
                //payFragment.closeDialogProgress();

                // Limpiar campos del fragmento Pagar
                //payFragment.clearFragment();

                // Limpiar campos del fragmento Carrito
                //shopCarFragment.clearFragment();

                // Moverse al tab "Productos"
                //viewPager.setCurrentItem(0,true);

                // Restaurar todos los productos en el arreglo
                //restoreProducts();

                // Limpiar datos generales del pedido
                //clearSale();
                ////this.clearProcess();
            }
        }else if(registerSaleResponse.code() == 401 || registerSaleResponse.code() == 400){
            String error = "";
            JSONObject jObjError = null;
            try {
                jObjError = new JSONObject(registerSaleResponse.errorBody().string());
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                error = jObjError.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Toasty.error(context,error).show();
            // Checar en caso de que sea guardado local
            if(isLocal){
                closeDialogProcessSynchronized(false);
                return;
            }

            payFragment.closeDialogProgress();
            confirmClearProcess();
            return;
        }
        else{
            Toasty.error(context, getString(R.string.msg_load_sale_fail)).show();
        }

        // Ocultar teclado del telefono
        payFragment.closeDialogProgress();
    }

    @Override
    public void responsePay(Response<RegisterPayResponse> registerPayResponseResponse, boolean onlyPay,boolean isLocal) {

        // Verificar que el pago regrese un response 200
        if(registerPayResponseResponse.isSuccessful()){

            // Mostrar mensaje  existo al realizar pago
            Toasty.success(context, getString(R.string.msg_load_pay_success)).show();

            // Checar en caso de que sea guardado local
            if(isLocal){
                responsePayWithOffLineAsyncTask(registerPayResponseResponse);
                return;
            }

            // Mandar a llamar a la impresora para dar comprobante de pago
            this.printerPay(registerPayResponseResponse.body());

        }else{

            // Mostrar mensaje de error al realizaro el pago
            Toasty.error(context, getString(R.string.msg_load_pay_fail)).show();

            // Checar en caso de que sea guardado local
            if(isLocal){
                closeDialogProcessSynchronized(true);
                return;
            }
        }
        if(!onlyPay){
            // Ocultar teclado del telefono
            payFragment.closeDialogProgress();

            // Limpiar campos del fragmento Pagar
            //payFragment.clearFragment();

            // Limpiar campos del fragmento Carrito
            //shopCarFragment.clearFragment();

            // Moverse al tab "Productos"
            //viewPager.setCurrentItem(0,true);

            // Restaurar todos los productos en el arreglo
            //restoreProducts();

            // Limpiar datos generales del pedido
            //clearSale();
            ////this.clearProcess();
        }else{
            // Actualizar la lista de pedidos
            //requestSalesLoaded();
        }
        clearProcess();
    }

    @Override
    public void responseSearchProducts(ArrayList<Product> products) {
        productFragment.showProgressBar(false);
        mLoadSearchProductAsyncTask = null;
        productFragment.getProductAdapter().updateData(products);
    }

    /**
     * Obtener el cliente seleccionado para levantar el pedido
     * @return
     */
    @Override
    public Customer getCustomerSelected() {
        return customerSelected;
    }

    @Override
    public SaleLoaded getSaleLoaded() {
        return saleLoaded;
    }

    @Override
    public void setAccountStateCustomer(AccountStateCustomer accountStateCustomer) {
        this.accountStateCustomer = accountStateCustomer;
    }

    @Override
    public AccountStateCustomer getAccountStateCustomer() {
        return accountStateCustomer;
    }

    /**
     * Restaurar todos los productos en el arreglo de productos
     */
    private void restoreProducts(){
        if(!mode_offline_app){
            for (Product product:productsAddedShopCar){
                products.add(product);
            }
            productFragment.setAllProducts(products);

        }else{
            getAllProductsFromDataBase();
        }

        // Limpiar arreglo de productos agregados en el carrito de compras
        productsAddedShopCar.clear();

        // Actualizar adaptador de productos
        productFragment.getProductAdapter().notifyDataSetChanged();
    }

    /**
     * Limpiar los datos de la aplicacion(cliente seleccionado, producto preseleccionado)
     */
    private void clearSale(){
        toolbar.setSubtitle("");
        customerSelected = null;
        productPreSelected = null;
    }
    /**
     * Ocultar teclado del telefono
     */
    public  void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Calcular el total del pedido
     * @return
     */
    private float calculateTotalOrder(){
        float total = 0;

        for (ProductAddedShopCar productAddedShopCar : shopCarFragment.getShopCar()){
               total += productAddedShopCar.getTotal();
        }

        return total;
    }

    /**
     * Obtener fecha actual del sistema
     * @return
     */
    private String currentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
        return  mdformat.format(calendar.getTime());
    }

    /**
     * Obtener el indice del arreglo de productos
     * @param productSearch
     * @return
     */
    private int getIndexProducts(Product productSearch){
        int index = 0;
        for(Product product : products){
            if(product.getId() == productSearch.getId()){
                break;
            }
            index++;
        }
        return index;
    }

    /**
     * Interface de comunicacion de LoadPayDialogFragment
     * @param quantity
     */
    @Override
    public void setQuantityToPayLoad(float quantity,int customerId,int saleId) {
        // Crear objeto Pago
        /*Pay pay = new PayWithSale();
        ((PayWithSale) pay).setSaleId(saleId);
        pay.setCustomerId(customerId);
        pay.setSubtotal(quantity);

        // Llamar asynctask para realizar pago
        LoadPayAsyncTask loadPayAsyncTask = new LoadPayAsyncTask(delegateMainActivity,pay,true);
        loadPayAsyncTask.execute();*/
    }

    private void clearProcess(){
        // Ocultar teclado del telefono
        //payFragment.closeDialogProgress();

        // Limpiar campos del fragmento Pagar
        payFragment.clearFragment();

        // Limpiar campos del fragmento Carrito
        shopCarFragment.clearFragment();

        // Moverse al tab "Productos"
        viewPager.setCurrentItem(0,true);

        // Restaurar todos los productos en el arreglo
        restoreProducts();

        // Limpiar datos generales del pedido
        clearSale();

        // Limpiar Cliente
        customerSelected = null;

        // Limpiar pedido cargado
        saleLoaded = null;
    }

    private void confirmClearProcess(){
        new AlertDialog.Builder(this)
                .setTitle("Confirmación")
                .setMessage("¿Desea limpiar el proceso?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        clearProcess();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewPager.setCurrentItem(2,true);
                    }
                }).show();
    }

    /**
     * Mandar a imprimir el ultimo pago realizado en el pedido
     */
    private void printerPay(RegisterPayResponse registerPayResponse) {
        try {
            printer.findBluetoothDevice();
            printer.openBluetoothPrinter();
            printer.printData(buildTicketDoPay(registerPayResponse));
            printer.disconnectBT();
        } catch (IOException | NullPointerException e) {
            // Mostrar mensaje de error
            Toasty.error(context, getString(R.string.error_msg_printer), Toast.LENGTH_SHORT, true).show();
        }
    }

    /**
     * Construir el comprobante del pago realizado
     */
    private String buildTicketDoPay(RegisterPayResponse registerPayResponse){
        StringBuilder builderTicket = new StringBuilder();

        Customer customer = accountStateCustomer.getCustomer();

        builderTicket.append("      ..:: Super Vero ::.. \n");
        builderTicket.append("Cliente : "+customer.getSocialName()+"\n");
        builderTicket.append("Correo : "+customer.getEmail()+"\n");
        builderTicket.append("Tel :"+customer.getPhone()+"\n");
        builderTicket.append("Ruta : "+MainActivity.route+"\n");
        builderTicket.append("Vendedor : "+MainActivity.userName+"\n");
        builderTicket.append("            Pagos      \n");

        builderTicket.append("\n");

        // Obtener la lista de los pagos realizados
        //ArrayList<Payment> payments = saleLoadedSelected.getPayments();

        builderTicket.append("Fecha de Pago : "+Utils.formatDate(registerPayResponse.getPaymentDate()));
        builderTicket.append("\n");

        builderTicket.append("Abono : "+Utils.formatMoney(registerPayResponse.getSubtotal()));
        builderTicket.append("\n");
        builderTicket.append("\n");

        builderTicket.append("Pagos : "+Utils.formatMoney(registerPayResponse.getPaymentsAmount()));
        builderTicket.append("\n");

        builderTicket.append("Saldo : "+Utils.formatMoney(registerPayResponse.getBalanceAmount()));
        builderTicket.append("\n");


        builderTicket.append("\n");
        builderTicket.append("        Firma del Vendedor\n\n\n");
        builderTicket.append("        _________________\n");
        builderTicket.append("        "+MainActivity.userName+"\n");

        return builderTicket.toString();
    }

    /**
     * Poner en modo fuera de linea(leer y almacenar desde la base datos la informacion)
     */
    private void setModeOffLine(boolean enable){
        // Poner ua bandera en la aplicacion para saber que me encuentro fuera de linea(sin internet) o caso contrario
        SuperVeroApp.changeModeOffLine(this,enable);

        // Mostrar el item en el menu principal
        synchronizeProductsItem.setVisible(enable);
    }

    /**
     * Descargar todos los productos y almacenar en el movil (base de datos)
     */
    private void downloadDataBase(){
        // Eliminar los registros anteriores de la base de datos local
        this.deleteAllProducts();

        // Eliminar los registros anteriores de la base de datos local
        this.deleteAllCustomers();

        // Eliminar los registros anteriores de la base de datos local
        this.deleteAllSalesLoaded();

        //this.deleteAllSales();

        // Eliminar todos los registros de los estados de cuenta de la base de datos local
        this.deleteAllAccountStateCustomer();

        // Solicitar al servidor la lista de productos para almacenarlas en la base de datos
        requestProducts();

    }



    /**
     * Solicitar al servidor la lista de todos los productos
     */
    private void requestProducts() {
        // Asignar un titulo al cuadro de dialogo
        mProgressDialogForModeOffLine.setTitle(getString(R.string.title_synchonize_products));

        // Mostrar cuadro de dialogo
        mProgressDialogForModeOffLine.show();

        // Llamar a una tarea asyncrona para descargar la base de datos del servidor
        LoadProductAsyncTask mLoadProductsTask = new LoadProductAsyncTask(delegateMainActivity);
        mLoadProductsTask.execute();
    }

    /**
     * Solicitar al servidor la lista de los clientes asociados a la ruta del venedodor
     */
    private void requestCustomers() {
        // Llamar a una tarea asyncrona para descargar la base de datos del servidor
        LoadCustomerToDBAsyncTask mLoadCustomerToDBAsyncTask = new LoadCustomerToDBAsyncTask(delegateMainActivity);
        mLoadCustomerToDBAsyncTask.execute();

        // Asignar un titulo al cuadro de dialogo
        mProgressDialogForModeOffLine.setTitle(getString(R.string.title_synchonize_customers));

        // Mostrar cuadro de dialogo
        mProgressDialogForModeOffLine.show();
    }

    /**
     * Solicitar al servidor la lista de los ventas cargados a la ruta del venedodor
     */
    private void requestSalesLoaded(){
        // Empezar transaccion
        realm.beginTransaction();

        // Obtener todos los clientes
        RealmResults<Customer> customers = realm.where(Customer.class).findAll();

        // Establecer el numero de veces a realizar el ws para ocultar el diaslogo
        countAccountStateCustomer = customers.size();

        // Iterar todos los clientes
        for(int i=0; i<customers.size();i++) {

            // Llamar a una tarea asyncrona para descargar la base de datos del servidor
            LoadAccountStateCustomerToDBAsyncTask mLoadAccountStateCustomerToDBAsyncTask = new LoadAccountStateCustomerToDBAsyncTask(delegateMainActivity,customers.get(i));
            mLoadAccountStateCustomerToDBAsyncTask.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        }

        // Finalizar transaccion
        realm.commitTransaction();

        // Asignar un titulo al cuadro de dialogo
        mProgressDialogForModeOffLine.setTitle(getString(R.string.title_synchonize_sales_loaded));

        // Mostrar cuadro de dialogo
        mProgressDialogForModeOffLine.show();

    }

    @Override
    public void responseProducts(Response<ArrayList<Product>> response) {
        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Crear una instancia de RealmList para pasar el array list obtenido desde retrofit
        RealmList<Product> products = new RealmList<Product>();

        // Copiar el arraylist a realmlist
        products.addAll(response.body());

        // Copiar a la base de datos toda la lista
        realm.copyToRealm(products);

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();

        // Cerrar el cuadro de dialogo
        mProgressDialogForModeOffLine.hide();

        // Mostrar mensaje de la ultima fecha de actualizacion de la base de datos
        Toasty.success(context, "Lista de productos actualizada al "+Utils.formatDateText(),Toast.LENGTH_LONG).show();

        // Recargar el recycle view con los nuevos datos de la base de datos
        this.getAllProductsFromDataBase();

        // Solicitar al servidor la lista de clientes para almacenarlas en la base de datos
        requestCustomers();
    }

    @Override
    public void responseCustomers(Response<ArrayList<Customer>> response) {
        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Crear una instancia de RealmList para pasar el array list obtenido desde retrofit
        RealmList<Customer> customers = new RealmList<Customer>();

        // Copiar el arraylist a realmlist
        customers.addAll(response.body());

        // Copiar a la base de datos toda la lista
        realm.copyToRealm(customers);

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();

        // Cerrar el cuadro de dialogo
        mProgressDialogForModeOffLine.hide();

        // Mostrar mensaje de la ultima fecha de actualizacion de la base de datos
        Toasty.success(context, "Lista de clientes actualizada al "+Utils.formatDateText(),Toast.LENGTH_LONG).show();

        // Solicitar al servidor la lista de ventas cargadas para almacenarlas en la base de datos
        requestSalesLoaded();
    }

    @Override
    public void responseSalesLoaded(Response<ArrayList<SaleLoaded>> response) {
        // Iniciar la transaccion de la base de datos
        /*realm.beginTransaction();

        // Crear una instancia de RealmList para pasar el array list obtenido desde retrofit
        RealmList<SaleLoaded> saleLoadeds = new RealmList<SaleLoaded>();

        // Copiar el arraylist a realmlist
        saleLoadeds.addAll(response.body());

        // Copiar a la base de datos toda la lista
        realm.copyToRealmOrUpdate(saleLoadeds);

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();

        // Cerrar el cuadro de dialogo
        mProgressDialogForModeOffLine.hide();

        // Mostrar mensaje de la ultima fecha de actualizacion de la base de datos
        Toasty.success(context, "Lista de ventas cargadas actualizada al "+Utils.formatDateText(),Toast.LENGTH_LONG).show();*/
    }

    @Override
    public void responseAccountStateCustomer(Response<AccountStateCustomer> accountStateCustomer,Customer customer) {
        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Dismuinuir el conteo de los estados de cuenta
        countAccountStateCustomer--;
        // Crear una instancia de RealmList para pasar el array list obtenido desde retrofit
        //RealmList<> saleLoadeds = new RealmList<SaleLoaded>();

        // Copiar el arraylist a realmlist
        //saleLoadeds.addAll(response.body());
        // Asignar un id autoincrementable
        AccountStateCustomer _accountStateCustomer = accountStateCustomer.body();
        _accountStateCustomer.setId(getPrimaryKeyAccountStateCustomerClass());
        _accountStateCustomer.setCustomer(customer);

        // Validar que los arreglos de venta y pagos no vengan vacios
        if(_accountStateCustomer.getPayOfCustomers().size()>0 || _accountStateCustomer.getSaleOfCustomers().size() >0) {

            // Copiar a la base de datos toda la lista
            realm.insertOrUpdate(_accountStateCustomer);
        }

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();

        // Validar si el contador de los estados de cuenta es igual a 1, para cerrar el dialogo y mostrar mensaje
        if(countAccountStateCustomer ==0) {
            // Cerrar el cuadro de dialogo
            mProgressDialogForModeOffLine.hide();

            // Mostrar mensaje de la ultima fecha de actualizacion de la base de datos
            Toasty.success(context, "Lista de ventas cargadas actualizada al " + Utils.formatDateText(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Eliminar todos los registros de la tabla "Products" de la base de datos local
     */
    private void deleteAllProducts(){
        // Obtener todos los productos de la base de datos
        RealmResults<Product> resultsProducts = realm.where(Product.class).findAll();

        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Eliminar todos los registros
        resultsProducts.deleteAllFromRealm();

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();
    }

    /**
     * Eliminar todos los registros de la tabla "Customers" de la base de datos local
     */
    private void deleteAllCustomers(){

        // Obtener todos los clientes de la base de datos
        RealmResults<Customer> resultsCustomers = realm.where(Customer.class).findAll();

        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Eliminar todos los registros
        resultsCustomers.deleteAllFromRealm();

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();
    }

    /**
     * Eliminar todos los registros de la tabla "Ventas Cargadas" de la base de datos local
     */
    private void deleteAllSalesLoaded(){

        // Obtener todos las ventas cargadas de la base de datos
        RealmResults<SaleLoaded> resultsSalesLoaded = realm.where(SaleLoaded.class).findAll();

        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Eliminar todos los registros
        resultsSalesLoaded.deleteAllFromRealm();

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();
    }

    /**
     * Eliminar todos las ventas
     */
    private void deleteAllSales(){
        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Obtener todos los productos de la base de datos
        RealmResults<Sale> resultsProducts = realm.where(Sale.class).findAll();

        // Eliminar todos los registros
        resultsProducts.deleteAllFromRealm();

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();
    }

    /**
     * Eliminar todos los estados de cuenta de los clientes
     */
    private void deleteAllAccountStateCustomer(){
        // Iniciar la transaccion de la base de datos
        realm.beginTransaction();

        // Obtener todos los productos de la base de datos
        RealmResults<AccountStateCustomer> resultsAccountStateCustomer = realm.where(AccountStateCustomer.class).findAll();

        // Eliminar todos los registros
        resultsAccountStateCustomer.deleteAllFromRealm();

        // Confirmar y cerrar la transaccion de la base de datos
        realm.commitTransaction();
    }


    /**
     * Configurar el cuadro de dialogo de progreso cuando se nesecite al momento de sincronizar datos
     * En modo fuera de linea
     */
    private void setupDialogProgressForModeOffLine(){
        mProgressDialogForModeOffLine  = new Dialog(context,android.R.style.Theme_DeviceDefault_Light_Dialog_Alert);
        mProgressDialogForModeOffLine.setCancelable(false);
        mProgressDialogForModeOffLine.setContentView(R.layout.loading_layout);
    }

    /**
     * Obtener todos los registros de productos desde la base de datos
     */
    public void getAllProductsFromDataBase(){
        Iterator<Product> productsDB = realm.where(Product.class).findAll().iterator();
        ArrayList<Product> products = new ArrayList<Product>();

        while (productsDB.hasNext()){
            products.add(productsDB.next());
        }
        productFragment.setAllProducts(products);

    }

    /**
     * Obtener productos con paginacion desde el servidor(teniendo acceso a internet)
     */
    private void getProductsFromServer(){
        productFragment.getProductAdapter().getProducts().clear();
        productFragment.getProductAdapter().getProductsFiltered().clear();
        productFragment.loadFirstPage();
    }

    public void saveOnlyPayToBD(Pay pay){
        realm.beginTransaction();
        pay.setId(getPrimaryKeyPayClass());
        realm.insert(pay);

        realm.commitTransaction();

        // Mandar a imprimir en automatico
        payFragment.printOrder();

        // Cerrar el cuadro de dialogo
        payFragment.closeDialogProgress();

        // Limpiar proceso
        clearProcess();

        this.invalidateOptionsMenu();// Actualizar las opciones del menu
    }

    /**
     * Guardar la venta en la base de datos
     */
    private void savePayToBD(int saleId){

        // Crear objeto Pago
        Pay pay = new Pay();
        pay.setId(getPrimaryKeyPayClass());
        pay.setSaleId(saleId);
        pay.setCustomerId(customerSelected.getId().intValue());
        pay.setSubtotal(Float.parseFloat(payFragment.getEtPayQuantityToPay().getText().toString()));
        pay.setCreatedById(Integer.parseInt(id));

        realm.insert(pay);

        realm.commitTransaction();

        // Mandar a imprimir en automatico
        payFragment.printOrder();

        // Cerrar el cuadro de dialogo
        payFragment.closeDialogProgress();

        // Limpiar proceso
        clearProcess();

        this.invalidateOptionsMenu();// Actualizar las opciones del menu

    }

    /**
     * Obtener el numero de la clase Sale
     *
     */
    private int getPrimaryKeySaleClass(){
        // increment index
        Number num = realm.where(Sale.class).max("id");
        int nextID;
        if(num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }
        return nextID;
    }

    /**
     * Obtener el numero de la clase SaleItem
     *
     */
    private int getPrimaryKeySaleItemClass(){
        // increment index
        Number num = realm.where(SaleItem.class).max("id");
        int nextID;
        if(num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }
        return nextID;
    }

    /**
     * Obtener el numero de la clase Pay
     *
     */
    private int getPrimaryKeyPayClass(){
        // increment index
        Number num = realm.where(Pay.class).max("id");
        int nextID;
        if(num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }
        return nextID;
    }

    /**
     * Obtener el numero de la clase AccountStateCustomer
     *
     */
    private int getPrimaryKeyAccountStateCustomerClass(){
        // increment index
        Number num = realm.where(AccountStateCustomer.class).max("id");
        int nextID;
        if(num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }
        return nextID;
    }


    /**
     * Checar si existen ventas o pagos pendientes por sincronizar
     * @return
     */
    private boolean existsSaleOrPayForSyncUp(){
        boolean exists = false;

        RealmResults<Sale> salesPendingForSyncUp = realm.where(Sale.class).findAll();
        RealmResults<Pay> paysPendingForSyncUp = realm.where(Pay.class).findAll();

        if(salesPendingForSyncUp.size() > 0 || paysPendingForSyncUp.size() > 0){
            exists = true;
        }
        return exists;
    }

    /**
     * Sincronizar las ventas
     * @return
     */
    private void syncUpSales(){
        realm.beginTransaction();
        RealmResults<Sale> salesPendingForSyncUp = realm.where(Sale.class).findAll();
        List<Sale> sales = realm.copyFromRealm(salesPendingForSyncUp);
        realm.commitTransaction();

        countSales = sales.size();

        mProgressDialogForModeOffLine.setTitle(getString(R.string.title_synchonize_sales));
        mProgressDialogForModeOffLine.show();
        if(sales.size() > 0) {
            for (Sale sale : sales) {
                LoadSaleAsyncTask loadSaleAsyncTask = new LoadSaleAsyncTask(delegateMainActivity, sale, true);
                loadSaleAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }else{
            mProgressDialogForModeOffLine.dismiss();
            syncUpPays();
        }
    }

    /**
     * Sincronizar las  pagos
     * @return
     */
    private void syncUpPays(){
        realm.beginTransaction();
        RealmResults<Pay> paysPendingForSyncUp = realm.where(Pay.class).findAll();
        List<Pay> pays = realm.copyFromRealm(paysPendingForSyncUp);
        realm.commitTransaction();

        countSales = pays.size();

        mProgressDialogForModeOffLine.setTitle(getString(R.string.title_synchonize_pays));
        mProgressDialogForModeOffLine.show();
        if(pays.size()>0){
            for(Pay pay : pays){
                LoadPayAsyncTask loadPayAsyncTask = new LoadPayAsyncTask(delegateMainActivity, pay,true,true);
                loadPayAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }else{
            mProgressDialogForModeOffLine.dismiss();
        }
    }


    private void closeDialogProcessSynchronized(boolean isEnd){
        if(countSales == 1){
            mProgressDialogForModeOffLine.dismiss();
            if(!isEnd) {
                syncUpPays();
            }
        }else{
            countSales--;
        }
    }

    /**
     * Respuesta de la sincronizacación cuando se almacenan en la base de datos Ventas
     */
    public void responseSaleWithOffLineAsyncTask(Response<SaleLoaded> response){
        if(response.isSuccessful()){
            SaleLoaded saleLoaded = response.body();
            realm.beginTransaction();
            Sale sale = realm.where(Sale.class)
                    .equalTo("CustomerId", saleLoaded.getCustomerId())
                    .findFirst();
            sale.deleteFromRealm();
            realm.commitTransaction();
            closeDialogProcessSynchronized(false);
        }
    }

    /**
     * Respuesta de la sincronizacación cuando se almacenan en la base de datos Pagos
     */
    public void responsePayWithOffLineAsyncTask(Response<RegisterPayResponse> response){
        if(response.isSuccessful()){
            RegisterPayResponse responsePay = response.body();
            realm.beginTransaction();
            Pay pay = realm.where(Pay.class)
                    .equalTo("CustomerId", responsePay.getCustomerId())
                    .findFirst();
            pay.deleteFromRealm();
            realm.commitTransaction();
            closeDialogProcessSynchronized(true);
            this.invalidateOptionsMenu();// Actualizar las opciones del menu
        }
    }


    @Override
    public void onChange(RealmResults<Product> products) {
        Log.i("acabo","skldlkdjsklja");
    }

    /**
     * Clase interna para sincronizar las ventas
     */

    /*public class LoadSaleWithOffLineAsyncTask extends AsyncTask<Void, Void, Void> {

        // API Service
        private Call<SaleLoaded> registerSaleResponse = null;

        private Sale sale;

        public LoadSaleWithOffLineAsyncTask(Sale sale){
            this.sale = sale;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            registerSaleResponse = SuperVeroApiAdapter.getApiService().doSale(sale);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            registerSaleResponse.enqueue(new Callback<SaleLoaded>() {
                @Override
                public void onResponse(Call<SaleLoaded> call, Response<SaleLoaded> response) {
                    responseSaleWithOffLineAsyncTask(response);
                }

                @Override
                public void onFailure(Call<SaleLoaded> call, Throwable t) {

                }
            });
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialogForModeOffLine.dismiss();
        }
    }

    /*@Override
    public void onBackPressed() {
        logout();
        return;
    }*/
}
