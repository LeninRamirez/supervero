package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;

import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.model.User;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginAsyncTask extends AsyncTask<Void, Void, Void> {

    // Delegate Activity and fragments
    private DelegateLoginActivity delegate = null;

    // API Service
    private Call<User> userCall = null;

    // Data login username
    private String userName;

    // Data login password
    private String password;

    // Construct
    public LoginAsyncTask(DelegateLoginActivity delegate, String userName, String password) {
        this.delegate = delegate;
        this.userName = userName;
        this.password = password;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(userCall == null) {
            userCall = SuperVeroApiAdapter.getApiService().signin(this.userName,this.password);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                delegate.responseLogin(response);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

        return null;
    }
}