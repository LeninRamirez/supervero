package lvrd.com.supervero.lvrd.com.supervero.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.model.PayOfCustomer;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class PayOfCustomerAdapter extends RecyclerView.Adapter<PayOfCustomerAdapter.PayOfCustomerViewHolder> {

    //Lista de pagos del cliente
    private ArrayList<PayOfCustomer> paysOfCustomer = null;

    // Context
    Context context;

    public PayOfCustomerAdapter(Context context,ArrayList<PayOfCustomer> paysOfCustomer) {
        this.context = context;
        this.paysOfCustomer = paysOfCustomer;
        //this.refactorizeId();
    }

    @Override
    public PayOfCustomerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // Cargar layout
        View view = LayoutInflater.from(context).inflate(R.layout.recycle_view_pay_of_customer_item,viewGroup,false);
        PayOfCustomerAdapter.PayOfCustomerViewHolder viewHolder = new PayOfCustomerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PayOfCustomerViewHolder payOfCustomerViewHolder, int i) {

        // Asociar los pagos
        payOfCustomerViewHolder.bind(paysOfCustomer.get(i));
    }

    @Override
    public int getItemCount() {
        return paysOfCustomer.size();
    }

    /*private void refactorizeId(){
        for(int i = 0; i<this.paysOfCustomer.size(); i++){
            this.paysOfCustomer.get(i).setId(i+1);
        }
    }*/

    public class PayOfCustomerViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.textViewPaymentDateOfCustomer)
        TextView textViewPaymentDateOfCustomer;

        @BindView(R.id.textViewSubTotalOfCustomer)
        TextView textViewSubTotalOfCustomer;

        @BindView(R.id.textViewIdPaymentOfCustomer)
        TextView textViewIdPaymentOfCustomer;

        public PayOfCustomerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(PayOfCustomer payOfCustomer){
            textViewIdPaymentOfCustomer.setText("No. "+payOfCustomer.getId()+"");
            textViewPaymentDateOfCustomer.setText(Utils.formatDate(payOfCustomer.getPaymentDate()));
            textViewSubTotalOfCustomer.setText(Utils.formatMoney(payOfCustomer.getSubTotal()));
        }
    }
}
