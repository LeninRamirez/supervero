package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.adapters.PayOfCustomerAdapter;
import lvrd.com.supervero.lvrd.com.supervero.adapters.SaleOfCustomerAdapter;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadPayAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.Pay;
import lvrd.com.supervero.lvrd.com.supervero.model.RegisterPayResponse;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountStateCustomerDialogFragment extends DialogFragment {

    @BindView(R.id.textViewNameCustomer)
    TextView textViewNameCustomer;

    @BindView(R.id.textViewSales)
    TextView textViewSales;

    @BindView(R.id.textViewPayments)
    TextView textViewPayments;

    @BindView(R.id.buttonAddPay)
    Button buttonAddPay;

    @BindView(R.id.textViewLimitMoney)
    TextView textViewLimitMoney;

    @BindView(R.id.editTextPayAccountStateCustomer)
    EditText editTextPayAccountStateCustomer;

    @BindView(R.id.buttonLoadPayToAccountStateCustomer)
    Button buttonLoadPayToAccountStateCustomer;

    /*@BindView(R.id.recyclerViewPaysOfCustomer)
    RecyclerView recyclerViewPaysOfCustomer;

    @BindView(R.id.recyclerViewSalesOfCustomer)
    RecyclerView recyclerViewSalesOfCustomer;*/

    private  static final String ACCOUNT_STATE_CUSTOMER = "accountStateCustomer";

    DelegateMainActivity delegateMainActivity;

    // Obtener los datos del estado de cuenta del cliente previamente seleccionado
    private AccountStateCustomer accountStateCustomer = null;

    public static AccountStateCustomerDialogFragment newInstance(AccountStateCustomer accountStateCustomer){

        AccountStateCustomerDialogFragment f = new AccountStateCustomerDialogFragment();

        Bundle args = new Bundle();
        args.putSerializable(ACCOUNT_STATE_CUSTOMER, (Serializable) accountStateCustomer);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //delegateDialog =  this;
        delegateMainActivity = (DelegateMainActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        accountStateCustomer = (AccountStateCustomer) getArguments().getSerializable(ACCOUNT_STATE_CUSTOMER);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.dialogfragment_load_account_state_customer,container,false);

        ButterKnife.bind(this, view);

        //SET TITLE DIALOG TITLE
        getDialog().setTitle("Estado de Cuenta del Cliente");

        textViewNameCustomer.setText(accountStateCustomer.getCustomer().getSocialName());

        textViewSales.setText(Utils.formatMoney(accountStateCustomer.getTotalSales()));

        textViewPayments.setText(Utils.formatMoney(accountStateCustomer.getTotalPayments()));

        textViewLimitMoney.setText(Utils.formatMoney(Float.parseFloat(accountStateCustomer.getCustomer().getLimitMoney())));

        /*PayOfCustomerAdapter payOfCustomerAdapter = new PayOfCustomerAdapter(getContext(),accountStateCustomer.getPayOfCustomers());

        recyclerViewPaysOfCustomer.setHasFixedSize(true); // Ajustar a un tamaño que no cambiara nunca
        recyclerViewPaysOfCustomer.setLayoutManager(new LinearLayoutManager(getContext())); // Asignar Layout
        recyclerViewPaysOfCustomer.setAdapter(payOfCustomerAdapter);  // Asignar adaptador a la lista

        SaleOfCustomerAdapter saleOfCustomerAdapter = new SaleOfCustomerAdapter(getContext(),accountStateCustomer.getSaleOfCustomers());
        recyclerViewSalesOfCustomer.setHasFixedSize(true); // Ajustar a un tamaño que no cambiara nunca
        recyclerViewSalesOfCustomer.setLayoutManager(new LinearLayoutManager(getContext())); // Asignar Layout
        recyclerViewSalesOfCustomer.setAdapter(saleOfCustomerAdapter);  // Asignar adaptador a la lista*/

        buttonAddPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextPayAccountStateCustomer.setVisibility(View.VISIBLE);
                buttonLoadPayToAccountStateCustomer.setVisibility(View.VISIBLE);
                buttonAddPay.setVisibility(View.GONE);
            }
        });

        buttonLoadPayToAccountStateCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextPayAccountStateCustomer.getText().toString().length() == 0) {
                    Toasty.error(getContext(), "Ingrese la cantidad").show();
                } else {

                    // Generar pago  para sumarle al estado de cuenta
                    Pay pay = new Pay();
                    pay.setSubtotal(Float.parseFloat(editTextPayAccountStateCustomer.getText().toString()));
                    pay.setCustomerId(accountStateCustomer.getCustomer().getId().intValue());
                    pay.setCreatedById(Integer.parseInt(MainActivity.id));
                    //pay.setSaleId(accountStateCustomer);

                    // Validar si la app esta fuera de linea
                    if(!MainActivity.mode_offline_app) {
                        LoadPayAsyncTask loadPayAsyncTask = new LoadPayAsyncTask(delegateMainActivity, pay, true);
                        loadPayAsyncTask.execute();
                    }else{


                        ((MainActivity)getActivity()).saveOnlyPayToBD(pay);
                    }
                    dismiss();


                    //pay.setCustomerId(Long.valueOf(accountStateCustomer.getCustomer().getId()).intValue());
                    //pay.setSubtotal(25.00f);

                    /*Call<JsonObject> payAccountStateCustomer = SuperVeroApiAdapter.getApiService().doPay(pay);
                    payAccountStateCustomer.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.isSuccessful()) {
                                Toasty.success(getContext(), "Se ha abonado un pago a tu cuenta").show();
                            } else {
                                Toasty.error(getContext(), "Problemas al cargar el pago").show();
                            }
                            dismiss();
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });*/
                }
            }
        });

        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        SaleAndPayOfCustomerFragment f = (SaleAndPayOfCustomerFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentSaleAndPayOfCustomer);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }
}
