package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;

import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Pay;
import lvrd.com.supervero.lvrd.com.supervero.model.RegisterPayResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadPayAsyncTask extends AsyncTask<Void,Void,Void> {

    // Delegate Activity and fragments
    private DelegateMainActivity delegateMainActivity = null;

    private Call<RegisterPayResponse> registerPayResponse = null;
    //private Call<ResponseBody> registerPayResponse = null;
    private Pay pay = null;

    private boolean onlyPay = false;

    private boolean isLocal = false;


    public LoadPayAsyncTask(DelegateMainActivity delegateMainActivity,Pay pay,boolean onlyPay) {
        this.delegateMainActivity =  delegateMainActivity;
        this.pay = pay;
        this.onlyPay = onlyPay;
    }

    public LoadPayAsyncTask(DelegateMainActivity delegateMainActivity,Pay pay,boolean onlyPay,boolean isLocal) {
        this.delegateMainActivity =  delegateMainActivity;
        this.pay = pay;
        this.onlyPay = onlyPay;
        this.isLocal = isLocal;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(registerPayResponse == null) {
            registerPayResponse = SuperVeroApiAdapter.getApiService().doPay(pay);
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        registerPayResponse.enqueue(new Callback<RegisterPayResponse>() {
            @Override
            public void onResponse(Call<RegisterPayResponse> call, Response<RegisterPayResponse> response) {
                delegateMainActivity.responsePay(response,onlyPay,isLocal);
            }

            @Override
            public void onFailure(Call<RegisterPayResponse> call, Throwable t) {

            }
        });
        return null;
    }
}
