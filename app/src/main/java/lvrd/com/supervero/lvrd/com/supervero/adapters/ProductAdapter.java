package lvrd.com.supervero.lvrd.com.supervero.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> /*implements Filterable*/ {

    // Layout item
    private static final int ITEM = 0;

    // Layout loading
    private static final int LOADING = 1;

    //Contexto de la aplicacion
    private Context context = null;

    // Layout item
    private int layout;

    //Lista de productos
    private ArrayList<Product> products = null;
    private ArrayList<Product> productsFiltered = null;

    // Interface de comunicion para los eventos Item Click
    private OnItemClickListener listener = null;

    // Shared preferences para obtener la URL del servicio Web
    SharedPreferences sharedPreferences = null;

    // Determinar si se encuentra cargando mas items
    private boolean isLoadingAdded = false;


    /**
     * Constructor
     * @param
     */
    public ProductAdapter(Context context,int layout,ArrayList<Product> products,SharedPreferences sharedPreferences,OnItemClickListener listener) {

        // Contexto
        this.context = context;

        // Layout
        this.layout = layout;

        // Lista de productos para hacer filtrado
        this.productsFiltered = products;

        // Lista de productos
        this.products = new ArrayList<Product>();
        this.products.addAll(this.productsFiltered);

        // Escuchador
        this.listener = listener;

        // Shared preferences
        this.sharedPreferences = sharedPreferences;
    }

    /**
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(viewGroup, inflater);
                break;
            case LOADING:
                View view = inflater.inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }
        return viewHolder;
    }

    /**
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final Product product = productsFiltered.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ProductViewHolder productViewHolder = (ProductViewHolder) viewHolder;
                productViewHolder.bind(product,listener);
                break;
            case LOADING:
                break;
        }
    }


    /**
     *
     * @return int
     */
    @Override
    public int getItemCount() {
        return productsFiltered.size();
        //return  products.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == productsFiltered.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /**
     * Regresar el layout de un item
     * @param parent
     * @param inflater
     * @return
     */
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View view = inflater.inflate(this.layout, parent, false);
        viewHolder = new ProductViewHolder(view);
        return viewHolder;
    }


    public void updateData(ArrayList<Product> products){

        // Lista de productos para hacer filtrado
        this.productsFiltered = products;

        // Lista de productos
        this.products = new ArrayList<Product>();
        this.products.addAll(this.productsFiltered);
        notifyDataSetChanged();
    }

    public void updateListFilter(ArrayList<Product> products){
        //this.products = new ArrayList<Product>();
        this.products.addAll(products);
        notifyDataSetChanged();
    }

    public ArrayList<Product> getProducts(){
        return products;
    }

    public ArrayList<Product> getProductsFiltered(){
        return productsFiltered;
    }

    /*@Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    //productsFiltered = products;
                } else {
                    ArrayList<Product> filteredList = new ArrayList<>();
                    for (Product row : products) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                   // productsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                //filterResults.values = productsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //productsFiltered = (ArrayList<Product>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }*/
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase();
        this.productsFiltered.clear();
        if (charText.length() == 0) {
            this.productsFiltered.addAll(products);
        } else {
            for (Product product : products) {
                if (product.getName().toLowerCase().contains(charText)) {
                    this.productsFiltered.add(product);
                }
            }
        }
        notifyDataSetChanged();
    }

    /*
      ---------------------------------- Helpers -----------------------------------------
     */
    public void addAll(ArrayList<Product> productsFiltered) {

        for (Product product : productsFiltered) {
            this.products.add(product);
            this.productsFiltered.add(product);
        }
        notifyDataSetChanged();
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        this.products.add(new Product());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = this.products.size() - 1;
        Product product = getItem(position);

        if (product != null) {
            this.products.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Product getItem(int position) {
        return this.products.get(position);
    }

    /*
        ----------------------- View Holders ----------------------------------
    */


    /**
     * Clase interna patron ViewHolder cuando se muestra el item
     */
     public class ProductViewHolder extends RecyclerView.ViewHolder{

        // Componentes de interfaz
        @BindView(R.id.cv_item)
        CardView informationCv;

        @BindView(R.id.tv_product_name)
        TextView nameTv;

        @BindView(R.id.tv_product_category)
        TextView categoryTv;

        @BindView(R.id.tv_product_quantity_)
        TextView quantityTv;

        @BindView(R.id.tv_product_sell_price)
        TextView sellPriceTv;

        @BindView(R.id.tv_product_buy_price)
        TextView buyPriceTv;

        @BindView(R.id.tv_product_unit)
        TextView unitTv;

        @BindView(R.id.iv_product)
        ImageView imageIv;

        @BindView(R.id.bt_add_shop_car)
        ImageButton addIb;

        /**
         * Constructor
         * @param view
         *
         */
        public ProductViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        /**
         * Envolver elementos del layout
         * @param product
         * @param listener
         */
        public void bind(Product product,OnItemClickListener listener){

            this.nameTv.setText(product.getName());
            this.sellPriceTv.setText(Utils.formatMoney(product.getSellPrice())+"");
            this.buyPriceTv.setText(Utils.formatMoney(product.getBuyPrice())+"");
            this.categoryTv.setText(product.getType());
            this.unitTv.setText(product.getUnit());
            this.quantityTv.setText(product.getStock()+"");
            // Cargamos la imagen con Picasso
            if(!TextUtils.isEmpty(product.getUrlImage())) {
                // Construir url para acceder a la imagen del producto
                String path = Utils.getSharedPreferences(sharedPreferences,context.getString(R.string.PREF_URL_WEB_SERVICE))+Utils.pathImage + product.getUrlImage();

                // Cargar imagen con libreria picasso para que se muestre en el layout del producto
                Picasso.get().load(path)
                        .centerCrop()
                        .fit()
                        .into(this.imageIv);
            }

            // Evento sobre el boton agregar producto del CardView
            this.addIb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(product,getAdapterPosition());
                }
            });
        }
    }

    /**
     * Clase interna patron ViewHolder cuando se encuentra cargando
     */
    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }


    /*
        Interface de comunicacion
     */

    /**
     * Interface de comunicacion entre el adaptador y fragmento
     **/
    public interface OnItemClickListener {
        void onItemClick(Product product, int position);
    }
}
