package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;
    private String Name;
    private String Type;
    private String Unit;
    private float SellPrice;
    @SerializedName("BuyPrice")
    private float buyPrice;
    private int Stock;
    @SerializedName("Quantity")
    private int quantity = 0;

    @SerializedName("Subtotal")
    private float subtotal;

    private boolean isAdded;

    @SerializedName("UrlPhoto")
    private String urlImage;

    public Product() {
    }

    public Product(int id, String name) {
        this.id = id;
        this.Name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public float getSellPrice() {
        return SellPrice;
    }

    public void setSellPrice(float sellPrice) {
        SellPrice = sellPrice;
    }

    public float getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(float buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int stock) {
        Stock = stock;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    /*@Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(!(obj instanceof Product)) return false;

        Product other = (Product) obj;

        if(id == null){
            if(other.id != null) return false;
        }
        else return id.equals(other.id);

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        if(id == null)
            hash = hash * 31;
        else
            hash = hash * 31 + id.hashCode();

        return hash;
    }*/

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", Name='" + Name + '\'' +
                ", Type='" + Type + '\'' +
                ", Unit='" + Unit + '\'' +
                ", SellPrice=" + SellPrice +
                ", Stock=" + Stock +
                ", quantity=" + quantity +
                ", isAdded=" + isAdded +
                '}';
    }
}
