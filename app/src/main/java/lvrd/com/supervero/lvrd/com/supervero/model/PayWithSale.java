package lvrd.com.supervero.lvrd.com.supervero.model;

import java.io.Serializable;

public class PayWithSale  implements Serializable {

    private int SaleId;

    public PayWithSale() {
    }

    public PayWithSale(float subtotal, int customerId, int saleId) {
        //super(subtotal, customerId);
        SaleId = saleId;
    }

    public int getSaleId() {
        return SaleId;
    }

    public void setSaleId(int saleId) {
        SaleId = saleId;
    }

    @Override
    public String toString() {
        return "PayWithOutSale{" +
                "SaleId=" + SaleId +
                '}';
    }
}
