package lvrd.com.supervero.lvrd.com.supervero.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.adapters.SeeProductAdapter;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeeProductsDialogFragment extends DialogFragment {

    private ArrayList<Product> products;

    private SeeProductAdapter seeProductAdapter;

    // Manager de visualizacion de los items del recycle view
    private RecyclerView.LayoutManager layoutManager;

    @BindView(R.id.recycleViewSeeProducts_dialogfragment_see_products)
    RecyclerView recyclerViewSeeProducts;

    @BindView(R.id.imageButtonClose_dialogfragment_see_products)
    ImageButton imageButtonClose;

    static SeeProductsDialogFragment newInstance(ArrayList<Product> products) {
        SeeProductsDialogFragment f = new SeeProductsDialogFragment();
        f.products=products;
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_see_products, container,
                false);
        ButterKnife.bind(this,view);

        seeProductAdapter = new SeeProductAdapter(this.products,R.layout.recycle_view_see_product_item,getContext());

        layoutManager = new LinearLayoutManager(getContext());

        recyclerViewSeeProducts.setLayoutManager(layoutManager);
        recyclerViewSeeProducts.setHasFixedSize(true);
        recyclerViewSeeProducts.setAdapter(seeProductAdapter);

        // Añade un efecto por defecto, si le pasamos null lo desactivamos por completo
        recyclerViewSeeProducts.setItemAnimator(new DefaultItemAnimator());


        return view;
    }

    @OnClick(R.id.imageButtonClose_dialogfragment_see_products)
    public void closeDialogFragment(){
        getDialog().dismiss();
    }

}
