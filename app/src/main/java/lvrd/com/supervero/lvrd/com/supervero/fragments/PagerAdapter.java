package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.HashSet;

import lvrd.com.supervero.R;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private Context context = null;

    private Resources resources =  null;

    private ArrayList<Fragment> fragments = null;

    public PagerAdapter(FragmentManager fm, Context context,ArrayList<Fragment> fragments) {
            super(fm);

            // Asignar contexto
            this.context = context;

            // Iniciar objeto resources
            resources = this.context.getResources();

            // Asignar fragmentos
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            return this.fragments.get(i);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return resources.getString(R.string.tab_product_name);
                case 1:
                    return resources.getString(R.string.tab_shopping_car_name);
                case 2:
                    return resources.getString(R.string.tab_pay_name);
                /*case 3:
                    return resources.getString(R.string.tab_sales_name);*/
            }
            return null;
        }
}