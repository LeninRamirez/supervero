package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadAccountStateCustomerAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.asynctask.LoadCustomerAsyncTask;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoadAccountStateCustomerDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateLoadCustomerDialogFragment;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.AccountStateCustomer;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;

public class SelectedCustomerForSeeAccountStateDialogFragment extends DialogFragment implements /*SearchView.OnQueryTextListener,*/DelegateLoadAccountStateCustomerDialogFragment,DelegateLoadCustomerDialogFragment {
    
    @BindView(R.id.rv_customers)
    RecyclerView customerRv;

    @BindView(R.id.pb_loading_customers)
    ProgressBar loadingCustomersPb;

    @BindView(R.id.bt_selected_customer)
    Button selectedCustomerBt;

    @BindView(R.id.searchViewCustomer)
    SearchView searchViewCustomer;

    // Tarea asincrona para realizar la carga de clientes
    private LoadCustomerAsyncTask mLoadCustomerAsyncTask = null;

    // Tarea asincrona para realizar la carga de clientes
    private LoadAccountStateCustomerAsyncTask mLoadAccountStateCustomerAsyncTask = null;

    SelectedCustomerAdapter selectedCustomerAdapter;

    private ArrayList<Customer> mCustomers = new ArrayList<Customer>();

    DelegateLoadAccountStateCustomerDialogFragment delegateDialog = null;

    DelegateLoadCustomerDialogFragment delegateLoadCustomerDialogFragment = null;

    DelegateMainActivity delegateMainActivity;

    // Instancia para la base de datos
    Realm realm = null;

    public static SelectedCustomerForSeeAccountStateDialogFragment newInstance(){
        SelectedCustomerForSeeAccountStateDialogFragment f = new SelectedCustomerForSeeAccountStateDialogFragment();

        Bundle args = new Bundle();
        //args.putSerializable(CUSTOMERS, (Serializable) customers);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delegateDialog =  this;
        delegateLoadCustomerDialogFragment = this;
        delegateMainActivity = (DelegateMainActivity) context;
        // Inicio la instancia para la base de datos
        Realm.init(context);
        // Obtener la instancia de la base de datos Realm
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //customers = (ArrayList<Customer>) getArguments().getSerializable(CUSTOMERS);

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.dialogfragment_customers,container,false);

        ButterKnife.bind(this, view);

        //SET TITLE DIALOG TITLE
        getDialog().setTitle("Clientes");

        // Asociar un administrador de layout
        customerRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        selectedCustomerAdapter = new SelectedCustomerAdapter(mCustomers,getActivity(),delegateMainActivity);
        customerRv.setAdapter(selectedCustomerAdapter);

        //customerSv.setOnQueryTextListener(this);

        if(!MainActivity.mode_offline_app) {
            if (mLoadCustomerAsyncTask == null) {
                mLoadCustomerAsyncTask = new LoadCustomerAsyncTask(delegateLoadCustomerDialogFragment);
                mLoadCustomerAsyncTask.execute();
            }
        }else{
            // Llamar a la base de datos
            getAllCustomersFromDataBase();
        }

        //BUTTON
        selectedCustomerBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //delegateMainActivity.selectedCustomer(selectedCustomerAdapter.getCustomerSelected());
                // Verificar si la app se encuentra en modo fuera de linea
                if(!MainActivity.mode_offline_app) {
                    if (mLoadAccountStateCustomerAsyncTask == null) {
                        mLoadAccountStateCustomerAsyncTask = new LoadAccountStateCustomerAsyncTask(delegateDialog, Long.valueOf(selectedCustomerAdapter.getCustomerSelected().getId()).intValue());
                        mLoadAccountStateCustomerAsyncTask.execute();
                    }
                }else{
                    delegateMainActivity.selectedCustomer(selectedCustomerAdapter.getCustomerSelected());
                    // Buscar en la base ded datos local
                    realm.beginTransaction();
                    Log.i("Customer",selectedCustomerAdapter.getCustomerSelected().getId()+"");
                    AccountStateCustomer accountStateCustomer = realm.where(AccountStateCustomer.class)
                            .equalTo("customer.id",selectedCustomerAdapter.getCustomerSelected().getId())
                            .findFirst();

                    RealmQuery<AccountStateCustomer> queryForUsern = realm
                            .where(AccountStateCustomer.class)
                            .equalTo("customer.id", selectedCustomerAdapter.getCustomerSelected().getId());


                    loadAccountStateCustomer(queryForUsern.findFirst());
                    //realm.commitTransaction();




                }
            }
        });

        // Search View Customer on ArrayList
        searchViewCustomer.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                selectedCustomerAdapter.getFilter().filter(newText.toLowerCase());
                return false;
            }
        });
        return view;

    }

    /*@Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        selectedCustomerAdapter.getFilter().filter(newText);
        return false;
    }*/

    /**
     * Mostrar el dialog fragment del estado de cuenta del cliente
     */
    private void showAccountStateCustomerDialogFragment(AccountStateCustomer accountStateCustomer){
        AccountStateCustomerDialogFragment accountStateCustomerDialogFragment = AccountStateCustomerDialogFragment.newInstance(accountStateCustomer);
        accountStateCustomerDialogFragment.show(getFragmentManager(),"accountStateCustomerDialogFragment");
    }


    @Override
    public void loadAccountStateCustomer(AccountStateCustomer accountStateCustomer) {
        // Asociar estado de cuenta
        AccountStateCustomer accountStateCustomer1 = accountStateCustomer;

        // Agregar al objeto el cliente
        accountStateCustomer1.setCustomer(selectedCustomerAdapter.getCustomerSelected());

        // Poner de manera global el estado de cuenta con el cliente seleccionado
        delegateMainActivity.setAccountStateCustomer(accountStateCustomer1);
        realm.commitTransaction();

        // Mandar a llamar al siguiente dialog fragment con la informacion del estado de cuenta del cliente
        showAccountStateCustomerDialogFragment(accountStateCustomer1);

        //Ocultar el Dialogfragment de la seleccion del cliente
        dismiss();
    }

    @Override
    public void loadCustomers(ArrayList<Customer> response) {
        loadingCustomersPb.setVisibility(View.INVISIBLE);
        searchViewCustomer.setVisibility(View.VISIBLE);
        for (Customer customer : response){
            mCustomers.add(customer);
        }
        selectedCustomerAdapter.notifyDataSetChanged();
    }

    /**
     * Obtener todos los registros de clientes desde la base de datos
     */
    private void getAllCustomersFromDataBase(){
        Iterator<Customer> customersDB = realm.where(Customer.class).findAll().iterator();
        ArrayList<Customer> customers = new ArrayList<Customer>();

        while (customersDB.hasNext()){
            customers.add(customersDB.next());
        }
        loadCustomers(customers);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
