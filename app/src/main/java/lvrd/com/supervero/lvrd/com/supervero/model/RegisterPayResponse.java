package lvrd.com.supervero.lvrd.com.supervero.model;

import java.io.Serializable;

public class RegisterPayResponse implements Serializable {

    private int id;
    private float Subtotal;
    private int CustomerId;
    private int SaleId;
    private String PaymentDate;
    private float PaymentsAmount;
    private float BalanceAmount;


    public RegisterPayResponse() {
    }

    public int getId() {
        return id;
    }

    public float getSubtotal() {
        return Subtotal;
    }

    public int getCustomerId() {
        return CustomerId;
    }

    public int getSaleId() {
        return SaleId;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public float getPaymentsAmount() {
        return PaymentsAmount;
    }

    public void setPaymentsAmount(float paymentsAmount) {
        PaymentsAmount = paymentsAmount;
    }

    public float getBalanceAmount() {
        return BalanceAmount;
    }

    public void setBalanceAmount(float balanceAmount) {
        BalanceAmount = balanceAmount;
    }

    @Override
    public String toString() {
        return "RegisterPayResponse{" +
                "id=" + id +
                ", Subtotal=" + Subtotal +
                ", CustomerId=" + CustomerId +
                ", SaleId=" + SaleId +
                ", PaymentDate='" + PaymentDate + '\'' +
                ", PaymentsAmount='" + PaymentsAmount + '\'' +
                ", BalanceAmount='" + BalanceAmount + '\'' +
                '}';
    }
}
