package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;

public class SelectedCustomerAdapter extends RecyclerView.Adapter<SelectedCustomerAdapter.CustomerViewHolder> implements Filterable {

    //Lista de productos
    private ArrayList<Customer> customers = null;

    // Lista de clientes para realizar filtrado
    private ArrayList<Customer> mCustomersFilterList;

    //Contexto de la aplicacion
    private Context context = null;

    private int mCheckedPostion = 0;


    private DelegateMainActivity delegateMainActivity = null;

    public SelectedCustomerAdapter(ArrayList<Customer> customers, Context context, DelegateMainActivity delegateMainActivity) {
        this.customers = customers;
        this.mCustomersFilterList = customers;
        this.context = context;
        this.delegateMainActivity = delegateMainActivity;
    }

    @Override
    public CustomerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_cardview_selected_customer,viewGroup,false);


        SelectedCustomerAdapter.CustomerViewHolder customerViewHolder = new SelectedCustomerAdapter.CustomerViewHolder(view);
        return customerViewHolder;
    }

    @Override
    public void onBindViewHolder(CustomerViewHolder customerViewHolder, int i) {
        Customer customer = mCustomersFilterList.get(i);

        customerViewHolder.tvSocialName.setText(customer.getSocialName());
        customerViewHolder.tvShortName.setText(customer.getShortName());
        customerViewHolder.tvRfc.setText(customer.getRFC());
        customerViewHolder.tvPhone.setText("Tel : "+customer.getPhone());
        customerViewHolder.tvEmail.setText("Email : "+customer.getEmail());
        customerViewHolder.rBSelected.setChecked(i == mCheckedPostion);
        customerViewHolder.rBSelected.setOnClickListener(v -> {
            mCheckedPostion = i;
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mCustomersFilterList.size();
    }





    public Customer getCustomerSelected() {
        return mCustomersFilterList.get(mCheckedPostion);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mCustomersFilterList = customers;
                } else {
                    ArrayList<Customer> filteredList = new ArrayList<Customer>();
                    for (Customer row : customers) {


                        if (row.getSocialName().toLowerCase().startsWith(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    mCustomersFilterList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mCustomersFilterList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mCustomersFilterList = (ArrayList<Customer>) results.values;
                notifyDataSetChanged();
            }
        };
    }

            /**
     * Clase interna
     */
     public class CustomerViewHolder extends RecyclerView.ViewHolder{

        // Componentes de interfaz
        @BindView(R.id.tv_selected_customer_social_name)
        TextView tvSocialName;

        @BindView(R.id.tv_selected_customer_short_name)
        TextView tvShortName;

        @BindView(R.id.tv_selected_customer_rfc)
        TextView tvRfc;

        @BindView(R.id.tv_selected_customer_phone)
        TextView tvPhone;

        @BindView(R.id.tv_selected_customer_email)
        TextView tvEmail;

        @BindView(R.id.rb_selected_customer_selected)
        RadioButton rBSelected;

        /**
         * Constructor
         * @param view
         *
         */
        public CustomerViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            // Evento sobre el elemento CardView
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                        mCheckedPostion = position;
                        notifyDataSetChanged();
                }
            });
        }
    }

}
