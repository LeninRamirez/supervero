package lvrd.com.supervero.lvrd.com.supervero.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class Utils {

    private static final NumberFormat numberFormat;
    private static final SimpleDateFormat dateFormat;
    private static final SimpleDateFormat dateFormatText;
    public static String pathImage = "uploads/files/productos/";

    static{
        numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatText = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
    }


    /**
     * Formato moneda
     * @param value
     * @return
     */
    public static String formatMoney(float value){
        return numberFormat.format(value);
    }

    /**
     * Formato fecha "yyyy-MM-dd"
     * @param value
     * @return
     */
    public static String formatDate(String value){
        String dateString = "";
        try{
            Date date = dateFormat.parse(value);
            dateString = dateFormat.format(date);
        } catch (ParseException  | NullPointerException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     * Formato fecha "EEE, d MMM yyyy HH:mm:ss"
     * @return
     */
    public static String formatDateText(){
        return dateFormatText.format(Calendar.getInstance().getTime());
    }

    /**
     *  Obtener shared preferences
     * @param key
     * @return
     */
    public static String getSharedPreferences(SharedPreferences sharedPreferences,String key){
        return sharedPreferences.getString(key,"");
    }
}
