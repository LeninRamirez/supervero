package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.adapters.ProductAdapter;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.listeners.PaginationScrollListener;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductPagination;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductFragment extends Fragment {

    // RecycleView de lista para productos
    @BindView(R.id.productsRv)
    RecyclerView productsRecycleView;

    @BindView(R.id.productsWaitprogressBar)
    ProgressBar productsWaitProgressBar;

    @BindView(R.id.textViewEmptyDB)
    TextView textViewEmptyDB;

    // Adaptador para la lista de productos
    private ProductAdapter productAdapter = null;

    // Lista de productos
    private ArrayList<Product> products = null;

    // Manejador del layout recycleview
    private LinearLayoutManager productLayoutManager = null;

    // Interface de comunicacion entre el fragment y "ProductAdapter"
    public DelegateMainActivity delegateMainActivity = null;

    // Shared Preferences
    SharedPreferences sharedPreferences = null;

    // Paginacion
    private static final int PAGE_START = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 5;
    private int currentPage = PAGE_START;

    private boolean isSearch = false;


    public  ProductFragment(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sharedPreferences = context.getSharedPreferences(getString(R.string.PREF_APP), Context.MODE_PRIVATE);
        try{
            delegateMainActivity = (DelegateMainActivity) getActivity();
        }catch (ClassCastException e){
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_product,container,false); // Inflar layout principal

        ButterKnife.bind(this, view);  // Envolver los componentes del layout

        products = new ArrayList<Product>(); // Iniciar arreglo de objetos producto

        productLayoutManager = new LinearLayoutManager(getContext()); // Instanciar manejador de layout del recycle view

        productAdapter = new ProductAdapter(getActivity(), R.layout.cardview_layout, products, sharedPreferences,new ProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Product product, int position) {
                delegateMainActivity.selectedProduct(product);
            }
        }); // Instanciar adaptador para la lista  de productos


        // Registrar observador al recycle view, este cargando los datos mostrar "progress bar"
        productAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if(products.size()==0){ // Si la lista esta vacia
                    if(!MainActivity.mode_offline_app) {
                        productsWaitProgressBar.setVisibility(View.VISIBLE); // Mostrar barra de progresp
                    }else{
                        textViewEmptyDB.setVisibility(View.VISIBLE);
                    }
                    productsRecycleView.setVisibility(View.GONE); // Ocultar RecycleView
                }else{
                    productsWaitProgressBar.setVisibility(View.GONE); // Ocultar barra de progreso
                    textViewEmptyDB.setVisibility(View.GONE);
                    productsRecycleView.setVisibility(View.VISIBLE); // Mostrar RecycleView
                }
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
            }
        });



        productsRecycleView.setHasFixedSize(true); // Ajustar a un tamaño que no cambiara nunca
        productsRecycleView.setLayoutManager(productLayoutManager); // Asignar Layout
        productsRecycleView.setAdapter(productAdapter);  // Asignar adaptador a la lista

        // Evento Scroll sobre RecycleView
        productsRecycleView.addOnScrollListener(new PaginationScrollListener(productLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 5;

                    // mocking network delay for API call
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadNextPage();
                        }
                    }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        productsWaitProgressBar.setVisibility(View.GONE); // Ocultar barra de progreso
        productsRecycleView.setVisibility(View.VISIBLE); // Mostrar RecycleView

        if(MainActivity.mode_offline_app){
            ((MainActivity) getActivity()).getAllProductsFromDataBase();
        }else {
            // Llamar los primeros 5 items
            loadFirstPage();
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void openDialog(Product product){
        AddProductDialogFragment addProductDialogFragment = AddProductDialogFragment.newInstance(product);
        addProductDialogFragment.show(getFragmentManager(),"BUYA");
    }


    public ProductAdapter getProductAdapter() {
        return productAdapter;
    }


    public void setAllProducts(ArrayList<Product> products){
        this.products = products;
        productAdapter.updateData(this.products);
    }

    /**
     * Muestra u oculta la barra de progreso mientras carga la informacioon o realiza una busqueda en el
     * Recycle view de los productos
     * @param isVisible
     */
    public void showProgressBar(boolean isVisible){
        if(isVisible) {
            productsWaitProgressBar.setVisibility(View.VISIBLE);
            productsRecycleView.setVisibility(View.GONE);
        }else{
            productsWaitProgressBar.setVisibility(View.GONE);
            productsRecycleView.setVisibility(View.VISIBLE);
        }
    }

    public void loadFirstPage() {
        textViewEmptyDB.setVisibility(View.GONE);
        currentPage = PAGE_START;
        isLastPage = false;
        callTopRatedProductsApi().enqueue(new Callback<ProductPagination>() {
            @Override
            public void onResponse(Call<ProductPagination> call, Response<ProductPagination> response) {
                ArrayList<Product> products = response.body().getProducts();
                TOTAL_PAGES = response.body().getTotalProducts()/ 5;
                productsWaitProgressBar.setVisibility(View.GONE);
                productAdapter.addAll(products);

                if (currentPage <= TOTAL_PAGES) productAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<ProductPagination> call, Throwable t) {

            }
        });
    }

    private void loadNextPage() {

        callTopRatedProductsApi().enqueue(new Callback<ProductPagination>() {
            @Override
            public void onResponse(Call<ProductPagination> call, Response<ProductPagination> response) {
                productAdapter.removeLoadingFooter();
                isLoading = false;

                ArrayList<Product> products = response.body().getProducts();
                productAdapter.addAll(products);

                if (currentPage != TOTAL_PAGES) productAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<ProductPagination> call, Throwable t) {

            }
        });
    }

    public boolean isSearch() {
        return isSearch;
    }

    public void setSearch(boolean search) {
        isSearch = search;
        if(!search){
            currentPage = PAGE_START;
        }
    }

    /**
     * Performs a Retrofit call to the top rated movies API.
     * Same API call for Pagination.
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<ProductPagination> callTopRatedProductsApi() {
        return SuperVeroApiAdapter.getApiService().getProductsPagination(currentPage,5);
    }

}
