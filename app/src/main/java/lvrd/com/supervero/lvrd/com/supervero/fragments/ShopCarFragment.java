package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.adapters.ShopCarAdapter;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import lvrd.com.supervero.lvrd.com.supervero.model.ProductAddedShopCar;
import ru.dimorinny.floatingtextbutton.FloatingTextButton;

/**
 * Fragment para el carro de ventas
 */
public class ShopCarFragment extends Fragment {

    @BindView(R.id.shopCarRv)
    RecyclerView rvShopCar = null;

    @BindView(R.id.fb_selected_customer)
    FloatingTextButton fbSelectedCustomer;

    @BindView(R.id.headerTitlesProductAddedToShopCarLineaLayout)
    LinearLayout headerTitlesProductAddedToShopCarLineaLayout;

    private ArrayList<ProductAddedShopCar> shopCar = null;

    private ShopCarAdapter shopCarAdapter;

    private DelegateMainActivity delegateMainActivity;

    public ShopCarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delegateMainActivity = (DelegateMainActivity) getActivity();
        try{
        }catch (ClassCastException e){
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Layout principal
        View view = inflater.inflate(R.layout.fragment_shop_car, container, false);
        ButterKnife.bind(this, view);

        //LAYOUT MANAGER
        rvShopCar.setLayoutManager(new LinearLayoutManager(getActivity()));
        //initData();
        //ADAPTER
        shopCar = new ArrayList<ProductAddedShopCar>();
        shopCarAdapter = new ShopCarAdapter(shopCar, getActivity(),delegateMainActivity, R.layout.layout_cardview_shop_car,new ShopCarAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ProductAddedShopCar productAddedShopCar, int position) {
            }
        });

        // Observador para el carrito de compras, cuando no haya nada cargado ocultar el boton de "Seleccionar Cliente" y
        // cabezeras de tabla de productos agregados al carro
        // Registrar observador al recycle view, este cargando los datos mostrar "progress bar"
        shopCarAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                //super.onChanged();
                if(shopCar.size()==0){ // Si la lista esta vacia
                    headerTitlesProductAddedToShopCarLineaLayout.setVisibility(View.GONE); // Mostrar barra de progresp
                    fbSelectedCustomer.setVisibility(View.GONE); // Ocultar RecycleView
                }else{
                    headerTitlesProductAddedToShopCarLineaLayout.setVisibility(View.VISIBLE); // Ocultar barra de progreso
                    fbSelectedCustomer.setVisibility(View.VISIBLE); // Mostrar RecycleView
                }
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
            }
        });
        rvShopCar.setAdapter(shopCarAdapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void addProduct(ProductAddedShopCar productAddedShopCar){

        ProductAddedShopCar  productAddedShopCar1 = existsProduct(productAddedShopCar);
        if(productAddedShopCar1 == null) {
            // Agregar producto al carro de venta
            shopCar.add(productAddedShopCar);

            // Refrescar recycleview
            shopCarAdapter.notifyDataSetChanged();
        }else{

            productAddedShopCar1.setQuantity(productAddedShopCar.getQuantity());
            shopCarAdapter.notifyDataSetChanged();
        }
    }

    public ArrayList<ProductAddedShopCar> getShopCar(){
        return shopCar;
    }

    @OnClick(R.id.fb_selected_customer)
    public void selectedCustomer(View view){
        SelectedCustomerDialogFragment selectedCustomerDialogFragment = SelectedCustomerDialogFragment.newInstance();
        selectedCustomerDialogFragment.show(getFragmentManager(),"BUYA");
    }

    public void clearFragment(){
        shopCar.clear();
        shopCarAdapter.notifyDataSetChanged();
    }

    private ProductAddedShopCar existsProduct(ProductAddedShopCar productAddedShopCar){
        ProductAddedShopCar __productAddedShopCar = null;
        for(ProductAddedShopCar _productAddedShopCar : shopCar){
            if(_productAddedShopCar.getId() == productAddedShopCar.getId()){
                __productAddedShopCar = _productAddedShopCar;
            }
        }
        return __productAddedShopCar;
    }

}
