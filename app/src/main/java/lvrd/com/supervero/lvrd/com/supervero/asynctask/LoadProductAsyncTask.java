package lvrd.com.supervero.lvrd.com.supervero.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

import io.realm.RealmResults;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.interfaces.DelegateMainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadProductAsyncTask extends AsyncTask<Void, Void, Void> {

    // Delegate Activity and fragments
    private DelegateMainActivity delegate = null;

    // API Service
    private Call<ArrayList<Product>> productCall = null;

    // Construct
    public LoadProductAsyncTask(DelegateMainActivity delegate) {
        this.delegate = delegate;;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(productCall == null) {
            productCall = SuperVeroApiAdapter.getApiService().getProducts();
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        productCall.enqueue(new Callback<ArrayList<Product>>() {
            @Override
            public void onResponse(Call<ArrayList<Product>> call, Response<ArrayList<Product>> response) {
                delegate.responseProducts(response);
            }

            @Override
            public void onFailure(Call<ArrayList<Product>> call, Throwable t) {

            }
        });

        return null;
    }
}
