package lvrd.com.supervero.lvrd.com.supervero.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.SystemClock;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.model.Pay;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.model.Sale;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleItem;

public class SuperVeroApp extends Application {

    // Bandera para saber si la app esta en modo offline o online
    public static boolean MODE_OFFLINE = false;

    // Campo auto incrementable de la base de datos para la tabla producto
    public static AtomicInteger ProductID = new AtomicInteger();

    // Campo auto incrementable de la base de datos para la tabla cliente
    public static AtomicInteger CustomerID = new AtomicInteger();

    // Campo auto incrementable de la base de datos para la tabla ventas
    public static AtomicInteger SaleID = new AtomicInteger();

    // Campo auto incrementable de la base de datos para la tabla ventas hijos
    public static AtomicInteger SaleItemID = new AtomicInteger();

    // Campo auto incrementable de la base de datos para la tabla pagos
    public static AtomicInteger PayID = new AtomicInteger();

    // Shared Preferences
    private static SharedPreferences appSharedPreferences = null;

    // Shared Preferences editor
    private static SharedPreferences.Editor editorSharedPreferences = null;






    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        // Llamar la configuracion de REALM
        setupRealmConfig();

        // Incrementar Id's
        Realm realm = Realm.getDefaultInstance();
        SaleID = getIdByTable(realm, Sale.class);
        SaleItemID = getIdByTable(realm, SaleItem.class);
        PayID = getIdByTable(realm,Pay.class);
        realm.close();

        // Este es solo para poder ver el Splash Screen durante 3 segundos
        SystemClock.sleep(6000);

        // Obtener las shared preferences de la app
        appSharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.PREF_APP),Context.MODE_PRIVATE);

        // Obtener el editor de las shared preferences de la app
        editorSharedPreferences = appSharedPreferences.edit();

    }

    /**
     * Configuracion inicial para la base de datos REALM
     */
    private void setupRealmConfig(){

        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("supervero.offiline")
                .schemaVersion(5)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }


    /**
     * Obtener el ultimo id de la llave primaria de cada tabla
     * @param realm
     * @param anyClass
     * @param <T>
     * @return
     */
    private <T extends RealmObject> AtomicInteger getIdByTable(Realm realm, Class<T> anyClass) {
        RealmResults<T> results = realm.where(anyClass).findAll();
        return (results.size() > 0) ? new AtomicInteger(results.max("id").intValue()) : new AtomicInteger();
    }

    /**
     * Guardar y almacenar el modo de la aplicacion "offline" o "online"
     * @param enable
     */
    public static void changeModeOffLine(Context context,boolean enable){

        // Cambiar bandera de referencia
        MODE_OFFLINE = enable;

        if(appSharedPreferences == null)
            appSharedPreferences = context.getSharedPreferences(context.getString(R.string.PREF_APP),Context.MODE_PRIVATE);

        if(editorSharedPreferences == null)
            editorSharedPreferences = appSharedPreferences.edit();

        // Almacenar en SharePreferences
        editorSharedPreferences.putBoolean(context.getString(R.string.PREF_MODE_APP), enable);
        editorSharedPreferences.commit();
    }
}
