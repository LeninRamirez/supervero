package lvrd.com.supervero.lvrd.com.supervero.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductPagination implements Serializable {

    @SerializedName("count")
    private int totalProducts;

    @SerializedName("products")
    private ArrayList<Product> products;

    public int getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(int totalProducts) {
        this.totalProducts = totalProducts;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "ProductPagination{" +
                "totalProducts=" + totalProducts +
                ", products=" + products +
                '}';
    }
}
