package lvrd.com.supervero.lvrd.com.supervero.interfaces;

import lvrd.com.supervero.lvrd.com.supervero.model.User;
import retrofit2.Response;

public interface DelegateLoginActivity {
    public abstract void responseLogin(Response<User> response);
}
