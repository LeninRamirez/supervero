package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.activities.MainActivity;
import lvrd.com.supervero.lvrd.com.supervero.adapters.SaleLoadedAdapter;
import lvrd.com.supervero.lvrd.com.supervero.api.SuperVeroApiAdapter;
import lvrd.com.supervero.lvrd.com.supervero.model.Customer;
import lvrd.com.supervero.lvrd.com.supervero.model.Payment;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.model.SaleLoaded;
import lvrd.com.supervero.lvrd.com.supervero.utils.Printer;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaleLoadedDialogFragment extends DialogFragment {

    // Componente recycle view para mostrar los pedidos cargados
    @BindView(R.id.recycleViewSaleLoaded)
    RecyclerView recyclerViewSaleLoaded;


    // Manager de visualizacion de los items del recycle view
    private RecyclerView.LayoutManager layoutManager;

    // Adaptor del recycle view de pedidos cargados
    private SaleLoadedAdapter saleLoadedAdapter;

    // Datos de los pedidos cargados en el sistema
    private ArrayList<SaleLoaded> salesLoaded = new ArrayList<SaleLoaded>();

    // Venta seleccionada
    private SaleLoaded saleLoadedSelected = null;

    // Objeto para imprimir tickets
    private Printer printer = null;

    // Context del fragment
    Context context;

    // Tarea asincrona para obtener los pedidos cargados
    private LoadSaleLoadedAsyncTask mLoadSaleLoadedAsyncTask;

    // Tarea asincrona para cambiar el estatus del pedido a entregado
    private ChangeStatusSaleAsyncTask mChangeStatusSaleAsyncTask;

    // Lista de los pedidos
    private ArrayList<SaleLoaded> saleLoadeds = null;


    /**
     * Paso de argumentos para el dialog fragment
     *
     * @return
     */
    public static SaleLoadedDialogFragment newInstance() {
        SaleLoadedDialogFragment f = new SaleLoadedDialogFragment();
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Generar contexto
        this.context = context;

        // Crear instancia para mandar a imprimir
        printer = new Printer((Activity)context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Layout principal
        View view = inflater.inflate(R.layout.fragment_sale_loaded,container,false);


        // Cargar Butter al fragment
        ButterKnife.bind(this,view);

        // Obtener todos los pedidos cargados en sistema
        salesLoaded = this.getAllSalesLoaded();

        // Iniciar layout manager para el recycle view(Normal)
        layoutManager = new LinearLayoutManager(getActivity());

        //  Iniciar adaptador
        saleLoadedAdapter = new SaleLoadedAdapter(salesLoaded, R.layout.recycle_view_sale_loaded_item, getActivity(),
            new SaleLoadedAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(SaleLoaded saleLoaded, int position) {
                }
             },
            new SaleLoadedAdapter.OnMenuItemClickListener() {
                @Override
                public void onMenuItemClick(SaleLoaded saleLoaded, int position,int opcionMenu) {

                    // Mandar a llamar a la impresora...
                    saleLoadedSelected = saleLoaded;

                    if(opcionMenu == MainActivity.MENU_LOAD_PAY) {
                        showDialogFrgamentLoadPay(saleLoadedSelected);
                    }else if (opcionMenu == MainActivity.MENU_SEE_PRODUCTS){
                        showDialogFrgamentSeeProducts(saleLoadedSelected);

                    }else if(opcionMenu == MainActivity.MENU_PRINTER_PAY){

                        // Imprimir Ticket del ultimo pago realizado
                        printerPay();

                    }else if(opcionMenu == MainActivity.MENU_DELIVERY_SALE){

                        // Cambiar el estado del pedido a "Entregado"
                        requestChangeStatusSale();

                    }
                }
        });



        // Dimension exactas de los items en el recycleview
        recyclerViewSaleLoaded.setHasFixedSize(true);

        // Asignar layout manager al recycle view
        recyclerViewSaleLoaded.setLayoutManager(layoutManager);

        // Añade un efecto por defecto, si le pasamos null lo desactivamos por completo
        recyclerViewSaleLoaded.setItemAnimator(new DefaultItemAnimator());

        // Asignar adapter al recycle view
        recyclerViewSaleLoaded.setAdapter(saleLoadedAdapter);

        // Asociar menu al fragment
        registerForContextMenu(view);

        requestSalesLoaded();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Obtener la lista de pedidos
     * @return
     */
    public ArrayList<SaleLoaded> getAllSalesLoaded(){
        return this.salesLoaded;
    }

    /**
     * Asignar lista de pedidos al recycleview
     * @param salesLoaded
     */
    public void setAllSalesLoaded(ArrayList<SaleLoaded> salesLoaded){
        this.salesLoaded.clear();
        this.salesLoaded = salesLoaded;
        saleLoadedAdapter.loadSalesLoaded(salesLoaded);

    }



    /**
     * Mostrar Dialogo para realizar pago al pedido
     */
    private void showDialogFrgamentLoadPay(SaleLoaded saleLoaded) {
        LoadPayDialogFragment loadPayDialogFragment = LoadPayDialogFragment.newInstance(saleLoaded);
        loadPayDialogFragment.show(getFragmentManager(), "dialogFragmentLoadPay");
    }

    /**
     * Mostrar Dialogo para ver los productos del pedido
     */
    private void showDialogFrgamentSeeProducts(SaleLoaded saleLoaded) {
        ArrayList<Product> products = new ArrayList<Product>();
        products.addAll(saleLoaded.getProductsOfSaleLoaded());
        SeeProductsDialogFragment seeProductsDialogFragment = SeeProductsDialogFragment.newInstance(products);
        seeProductsDialogFragment.show(getFragmentManager(), "dialogFragmentSeeProducts");
    }

    /**
     * Construir el comprobante de ultimo pago
     */
    private String buildTicketDoPay(){
        StringBuilder builderTicket = new StringBuilder();

        // Obtener cliente seleccionado para levantar el pedido
        //Customer customer = delegateMainActivity.getCustomerSelected();

        // Obtener respuesta del servidor al cargar el pedido
        //SaleLoaded saleLoaded = delegateMainActivity.getSaleLoaded();

        Customer customer = saleLoadedSelected.getCustomer();

        builderTicket.append("      ..:: Super Vero ::.. \n");
        builderTicket.append("Cliente : "+customer.getSocialName()+"\n");
        builderTicket.append("Correo : "+customer.getEmail()+"\n");
        builderTicket.append("Tel :"+customer.getPhone()+"\n");
        builderTicket.append("Ruta : "+MainActivity.route+"\n");
        builderTicket.append("Vendedor : "+MainActivity.userName+"\n");
        builderTicket.append("            Pagos      \n");

        builderTicket.append("\n");
        builderTicket.append("PEDIDO # : "+saleLoadedSelected.getId()+"\n");
        builderTicket.append("\n");

        // Obtener la lista de los pagos realizados
        ArrayList<Payment> payments = new ArrayList<Payment>();
        payments.addAll(saleLoadedSelected.getPayments());

        builderTicket.append("Fecha de Pago : "+Utils.formatDate(payments.get((payments.size())-1).getCreatedAt()));
        builderTicket.append("\n");

        builderTicket.append("Abono : "+Utils.formatMoney(payments.get((payments.size())-1).getSubTotal()));
        builderTicket.append("\n");
        builderTicket.append("\n");

        builderTicket.append("Pagos : "+Utils.formatMoney(calculateTotalOfPayments(payments)));
        builderTicket.append("\n");

        builderTicket.append("Saldo : "+Utils.formatMoney(saleLoadedSelected.getTotalAmount()-calculateTotalOfPayments(payments)));
        builderTicket.append("\n");


        builderTicket.append("\n");
        builderTicket.append("        Firma del Vendedor\n\n\n");
        builderTicket.append("        _________________\n");
        builderTicket.append("        "+MainActivity.userName+"\n");

        return builderTicket.toString();
    }

    private String buildTicketDeliverySale(){
        StringBuilder builderTicket = new StringBuilder();

        // Obtener cliente seleccionado para levantar el pedido
        Customer customer = saleLoadedSelected.getCustomer();

        // Obtener respuesta del servidor al cargar el pedido
        SaleLoaded saleLoaded = saleLoadedSelected;

        builderTicket.append("      ..:: Super Vero ::.. \n");
        builderTicket.append("Cliente : "+customer.getSocialName()+"\n");
        builderTicket.append("Correo : "+customer.getEmail()+"\n");
        builderTicket.append("Tel :"+customer.getPhone()+"\n");
        builderTicket.append("Ruta : "+MainActivity.route+"\n");
        builderTicket.append("Vendedor : "+MainActivity.userName+"\n");
        builderTicket.append("Fecha del Pedido : "+Utils.formatDate(saleLoaded.getRealDate())+"\n");
        builderTicket.append("\n");
        builderTicket.append("PEDIDO # : "+saleLoaded.getId()+"\n");
        builderTicket.append("\n");
        for(Product productAddedShopCar :  saleLoadedSelected.getProductsOfSaleLoaded()){
            builderTicket.append("[  ]");
            builderTicket.append(String.format("%3d",productAddedShopCar.getQuantity()));
            builderTicket.append("  ");
            builderTicket.append(String.format("%5s",productAddedShopCar.getName()));
            builderTicket.append(" ");
            builderTicket.append(Utils.formatMoney(productAddedShopCar.getSellPrice()));
            builderTicket.append(" ");
            builderTicket.append(Utils.formatMoney(productAddedShopCar.getSubtotal()));
            builderTicket.append("\n");
        }

        builderTicket.append("\n");

        float subTotal = (saleLoaded.getTotalAmount()*25)/29;

        builderTicket.append("SubTotal :"+Utils.formatMoney(subTotal)+"\n");
        builderTicket.append("Iva :"+Utils.formatMoney((subTotal*Float.parseFloat("0.16")))+"\n");
        builderTicket.append("Total :"+Utils.formatMoney(saleLoaded.getTotalAmount())+"\n");

        builderTicket.append("\n");

        builderTicket.append("\n");



        builderTicket.append("\n");
        builderTicket.append("        Firma del Cliente\n\n\n");
        builderTicket.append("        _________________\n");

        return builderTicket.toString();
    }


    private float calculateTotalOfPayments(ArrayList<Payment> payments){

        float totalPayments = 0f;

        for(Payment payment : payments){
            totalPayments += payment.getSubTotal();
        }

        return totalPayments;
    }

    /**
     * Mandar a imprimir el ultimo pago realizado en el pedido
     */
    private void printerPay() {
        try {
            printer.findBluetoothDevice();
            printer.openBluetoothPrinter();
            printer.printData(buildTicketDoPay());
            printer.disconnectBT();
        } catch (IOException | NullPointerException e) {
            // Mostrar mensaje de error
            Toasty.error(context, getString(R.string.error_msg_printer), Toast.LENGTH_SHORT, true).show();
        }
    }

    /**
     * Mandar a imprimir la entrega del pedido
     */
    private void printerDeliverySale() {
        try {
            printer.findBluetoothDevice();
            printer.openBluetoothPrinter();
            printer.printData(buildTicketDeliverySale());
            printer.disconnectBT();
        } catch (IOException | NullPointerException e) {
            // Mostrar mensaje de error
            Toasty.error(context, getString(R.string.error_msg_printer), Toast.LENGTH_SHORT, true).show();
        }
    }

    /**
     * Solicitar los pedidos
     */
    private void requestSalesLoaded(){
        if(mLoadSaleLoadedAsyncTask == null){
            mLoadSaleLoadedAsyncTask = new LoadSaleLoadedAsyncTask();
            mLoadSaleLoadedAsyncTask.execute();
        }
    }

    private void requestChangeStatusSale(){
        if(mChangeStatusSaleAsyncTask == null){
            mChangeStatusSaleAsyncTask = new ChangeStatusSaleAsyncTask();
            mChangeStatusSaleAsyncTask.execute();
        }
    }

    /**
     * CLASE INTERNA PARA CARGAR PEDIDOS
     */
    public class LoadSaleLoadedAsyncTask extends AsyncTask<Void,Void,Void> {

        private Call<ArrayList<SaleLoaded>> salesLoadedCall =  null;

        public LoadSaleLoadedAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(salesLoadedCall == null){
                salesLoadedCall = SuperVeroApiAdapter.getApiService().getSalesLoadedByRoute(MainActivity.route);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            salesLoadedCall.enqueue(new Callback<ArrayList<SaleLoaded>>() {
                @Override
                public void onResponse(Call<ArrayList<SaleLoaded>> call, Response<ArrayList<SaleLoaded>> response) {
                    setAllSalesLoaded(response.body());
                    mLoadSaleLoadedAsyncTask = null;
                }

                @Override
                public void onFailure(Call<ArrayList<SaleLoaded>> call, Throwable t) {
                    Log.i("Error",t.toString());
                    mLoadSaleLoadedAsyncTask = null;
                }
            });
            return null;
        }
    }

    /**
     * CLASE INTERNA CAMBIAR EL ESTATUS DEL PEDIDO A ENTREGADO
     */
    public class ChangeStatusSaleAsyncTask extends AsyncTask<Void,Void,Void> {

        private Call<JsonObject> changeStatusSaleCall =  null;

        public ChangeStatusSaleAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(changeStatusSaleCall == null){
                changeStatusSaleCall = SuperVeroApiAdapter.getApiService().changeStatusSale(saleLoadedSelected.getId());
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            changeStatusSaleCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if(response.code() == 200){
                        Toasty.success(context, getString(R.string.msg_change_status_sale), Toast.LENGTH_SHORT, true).show();
                        printerDeliverySale();
                        requestSalesLoaded();
                    }else{
                        Toasty.error(context, "No se pudo cambiar el estado del pedido", Toast.LENGTH_SHORT, true).show();
                    }
                    mChangeStatusSaleAsyncTask = null;
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Toasty.error(context, "No se pudo cambiar el estado del pedido", Toast.LENGTH_SHORT, true).show();
                    mChangeStatusSaleAsyncTask = null;
                }
            });
            return null;
        }
    }
}
