package lvrd.com.supervero.lvrd.com.supervero.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lvrd.com.supervero.R;
import lvrd.com.supervero.lvrd.com.supervero.model.Product;
import lvrd.com.supervero.lvrd.com.supervero.utils.Utils;

public class AddProductDialogFragment extends DialogFragment  {

    public final static String PRODUCT = "product";

    @BindView(R.id.iv_add_product_image)
    ImageView ivProductImage;

    @BindView(R.id.tv_add_product_name)
    TextView tvProductName;

    @BindView(R.id.tv_add_product_sell_price)
    TextView tvProductSellPrice;

    @BindView(R.id.tv_add_product_stock)
    TextView tvProductStock;

    @BindView(R.id.til_add_product_quantity)
    TextInputLayout tilProductQuantity;

    @BindView(R.id.et_add_product_quantity)
    EditText etQuantity;

    @BindView(R.id.bt_add_product_to_car_shop_dialog_fragment)
    Button btAddProductToCarShop;

    @BindView(R.id.bt_cancel_product_to_car_shop_dialog_fragment)
    Button btCancelProductToCarShop;

    DelegateActivityDialogFragment delegateActivityDialogFragment;

    private Product mProduct;

    NumberFormat format;

    private int quantity = 0;

    // 1. Defines the listener interface with a method passing back data result.
    public interface DelegateActivityDialogFragment {
        void quiantityProducts(int count);
    }

    /**
     * Paso de argumentos para el dialog fragment
     * @param product
     * @return
     */
    static AddProductDialogFragment newInstance(Product product) {
        AddProductDialogFragment f = new AddProductDialogFragment();

        Bundle args = new Bundle();
        args.putSerializable(PRODUCT, product);
        f.setArguments(args);
        return f;
    }

    /**
     * Ciclo de vida del dialog fragment
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * Ciclo de vida del dialog fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Serializable product = getArguments().getSerializable(PRODUCT);
        mProduct = (Product) product;
    }

    /**
     * Ciclo de vida del dialog fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogfragment_add_product,container,false);
        ButterKnife.bind(this, view);
        delegateActivityDialogFragment = (DelegateActivityDialogFragment) getActivity();
        loadData(mProduct);
        return view;
    }

    /**
     * Ciclo de vida del dialog fragment
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * Escuchador de evento para los botones Aceptar o Cancelar
     * @param v
     */
    @OnClick({R.id.bt_add_product_to_car_shop_dialog_fragment,R.id.bt_cancel_product_to_car_shop_dialog_fragment})
    public void onClick(View v) {
        if(v.getId() == btAddProductToCarShop.getId()){
            if(TextUtils.isEmpty(etQuantity.getText().toString())){

                // Mostrar en la eiqueta mensaje de validacion
                tilProductQuantity.setError(getString(R.string.error_quantity_empty));
                tilProductQuantity.requestFocus();
                return;
            }
            int quantity = Integer.parseInt(etQuantity.getText().toString());

            if(quantity == 0){
                tilProductQuantity.setError(getString(R.string.error_quantity_zero));
                tilProductQuantity.requestFocus();
                return;
            }

            delegateActivityDialogFragment.quiantityProducts(quantity);
            getDialog().dismiss();
        }
        if(v.getId() == btCancelProductToCarShop.getId()){
            getDialog().dismiss();
        }
    }

    /**
     * Carga los datos en los widgets del  layout
     * @param product
     */
    private void loadData(Product product){

        // Asignar datos a los TextView
        tvProductName.setText(product.getName()+"("+product.getType()+")");
        tvProductSellPrice.setText(Utils.formatMoney(product.getSellPrice()));
        tvProductStock.setText(getString(R.string.current_existence)+String.valueOf(product.getStock())+" "+product.getUnit());

        // Verificar que el producto cuente con imagen
        if(!TextUtils.isEmpty(product.getUrlImage())){
            Picasso.get().load("http://173.249.17.194:5001/uploads/files/productos/"+product.getUrlImage())
                    .into(ivProductImage);
        }
    }
}
